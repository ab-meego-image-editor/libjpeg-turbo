/*
 * jidctfst.c
 *
 * Copyright (C) 1994-1998, Thomas G. Lane.
 *
 * ARM NEON optimizations
 * Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.
 * Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
 *
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file contains a fast, not so accurate integer implementation of the
 * inverse DCT (Discrete Cosine Transform).  In the IJG code, this routine
 * must also perform dequantization of the input coefficients.
 *
 * A 2-D IDCT can be done by 1-D IDCT on each column followed by 1-D IDCT
 * on each row (or vice versa, but it's more convenient to emit a row at
 * a time).  Direct algorithms are also available, but they are much more
 * complex and seem not to be any faster when reduced to code.
 *
 * This implementation is based on Arai, Agui, and Nakajima's algorithm for
 * scaled DCT.  Their original paper (Trans. IEICE E-71(11):1095) is in
 * Japanese, but the algorithm is described in the Pennebaker & Mitchell
 * JPEG textbook (see REFERENCES section in file README).  The following code
 * is based directly on figure 4-8 in P&M.
 * While an 8-point DCT cannot be done in less than 11 multiplies, it is
 * possible to arrange the computation so that many of the multiplies are
 * simple scalings of the final outputs.  These multiplies can then be
 * folded into the multiplications or divisions by the JPEG quantization
 * table entries.  The AA&N method leaves only 5 multiplies and 29 adds
 * to be done in the DCT itself.
 * The primary disadvantage of this method is that with fixed-point math,
 * accuracy is lost due to imprecise representation of the scaled
 * quantization values.  The smaller the quantization table entry, the less
 * precise the scaled value, so this implementation does worse with high-
 * quality-setting files than with low-quality ones.
 */

#define JPEG_INTERNALS
#include "jinclude.h"
#include "jpeglib.h"
#include "jdct.h"		/* Private declarations for DCT subsystem */

#ifdef DCT_IFAST_SUPPORTED


/*
 * This module is specialized to the case DCTSIZE = 8.
 */

#if DCTSIZE != 8
  Sorry, this code only copes with 8x8 DCTs. /* deliberate syntax err */
#endif


/* Scaling decisions are generally the same as in the LL&M algorithm;
 * see jidctint.c for more details.  However, we choose to descale
 * (right shift) multiplication products as soon as they are formed,
 * rather than carrying additional fractional bits into subsequent additions.
 * This compromises accuracy slightly, but it lets us save a few shifts.
 * More importantly, 16-bit arithmetic is then adequate (for 8-bit samples)
 * everywhere except in the multiplications proper; this saves a good deal
 * of work on 16-bit-int machines.
 *
 * The dequantized coefficients are not integers because the AA&N scaling
 * factors have been incorporated.  We represent them scaled up by PASS1_BITS,
 * so that the first and second IDCT rounds have the same input scaling.
 * For 8-bit JSAMPLEs, we choose IFAST_SCALE_BITS = PASS1_BITS so as to
 * avoid a descaling shift; this compromises accuracy rather drastically
 * for small quantization table entries, but it saves a lot of shifts.
 * For 12-bit JSAMPLEs, there's no hope of using 16x16 multiplies anyway,
 * so we use a much larger scaling factor to preserve accuracy.
 *
 * A final compromise is to represent the multiplicative constants to only
 * 8 fractional bits, rather than 13.  This saves some shifting work on some
 * machines, and may also reduce the cost of multiplication (since there
 * are fewer one-bits in the constants).
 */

#if BITS_IN_JSAMPLE == 8
#define CONST_BITS  8
#define PASS1_BITS  2
#else
#define CONST_BITS  8
#define PASS1_BITS  1		/* lose a little precision to avoid overflow */
#endif

/* Some C compilers fail to reduce "FIX(constant)" at compile time, thus
 * causing a lot of useless floating-point operations at run time.
 * To get around this we use the following pre-calculated constants.
 * If you change CONST_BITS you may want to add appropriate values.
 * (With a reasonable C compiler, you can just rely on the FIX() macro...)
 */

#if CONST_BITS == 8
#define FIX_1_082392200  ((INT32)  277)		/* FIX(1.082392200) */
#define FIX_1_414213562  ((INT32)  362)		/* FIX(1.414213562) */
#define FIX_1_847759065  ((INT32)  473)		/* FIX(1.847759065) */
#define FIX_2_613125930  ((INT32)  669)		/* FIX(2.613125930) */
#else
#define FIX_1_082392200  FIX(1.082392200)
#define FIX_1_414213562  FIX(1.414213562)
#define FIX_1_847759065  FIX(1.847759065)
#define FIX_2_613125930  FIX(2.613125930)
#endif


/* We can gain a little more speed, with a further compromise in accuracy,
 * by omitting the addition in a descaling shift.  This yields an incorrectly
 * rounded result half the time...
 */

#ifndef USE_ACCURATE_ROUNDING
#undef DESCALE
#define DESCALE(x,n)  RIGHT_SHIFT(x, n)
#endif


/* Multiply a DCTELEM variable by an INT32 constant, and immediately
 * descale to yield a DCTELEM result.
 */

#define MULTIPLY(var,const)  ((DCTELEM) DESCALE((var) * (const), CONST_BITS))


/* Dequantize a coefficient by multiplying it by the multiplier-table
 * entry; produce a DCTELEM result.  For 8-bit data a 16x16->16
 * multiplication will do.  For 12-bit data, the multiplier table is
 * declared INT32, so a 32-bit multiply will be used.
 */

#if BITS_IN_JSAMPLE == 8
#define DEQUANTIZE(coef,quantval)  (((IFAST_MULT_TYPE) (coef)) * (quantval))
#else
#define DEQUANTIZE(coef,quantval)  \
	DESCALE((coef)*(quantval), IFAST_SCALE_BITS-PASS1_BITS)
#endif


/* Like DESCALE, but applies to a DCTELEM and produces an int.
 * We assume that int right shift is unsigned if INT32 right shift is.
 */

#ifdef RIGHT_SHIFT_IS_UNSIGNED
#define ISHIFT_TEMPS	DCTELEM ishift_temp;
#if BITS_IN_JSAMPLE == 8
#define DCTELEMBITS  16		/* DCTELEM may be 16 or 32 bits */
#else
#define DCTELEMBITS  32		/* DCTELEM must be 32 bits */
#endif
#define IRIGHT_SHIFT(x,shft)  \
    ((ishift_temp = (x)) < 0 ? \
     (ishift_temp >> (shft)) | ((~((DCTELEM) 0)) << (DCTELEMBITS-(shft))) : \
     (ishift_temp >> (shft)))
#else
#define ISHIFT_TEMPS
#define IRIGHT_SHIFT(x,shft)	((x) >> (shft))
#endif

#ifdef USE_ACCURATE_ROUNDING
#define IDESCALE(x,n)  ((int) IRIGHT_SHIFT((x) + (1 << ((n)-1)), n))
#else
#define IDESCALE(x,n)  ((int) IRIGHT_SHIFT(x, n))
#endif


/*
 * Perform dequantization and inverse DCT on one block of coefficients.
 */

#if defined(WITH_SIMD) && defined(__ARM_NEON__) && (BITS_IN_JSAMPLE == 8)

#define XFIX_1_082392200 ((short)(277 * 128 - 256 * 128))
#define XFIX_1_414213562 ((short)(362 * 128 - 256 * 128))
#define XFIX_1_847759065 ((short)(473 * 128 - 256 * 128))
#define XFIX_2_613125930 ((short)(669 * 128 - 512 * 128))

GLOBAL(void)
jpeg_idct_ifast_neon (j_decompress_ptr cinfo, jpeg_component_info * compptr,
		      JCOEFPTR coef_block,
		      JSAMPARRAY output_buf, JDIMENSION output_col)
{
  JCOEFPTR inptr;
  IFAST_MULT_TYPE * quantptr;
  int tmp;

  const static short c[4] = {
    XFIX_1_082392200, /* d0[0] */
    XFIX_1_414213562, /* d0[1] */
    XFIX_1_847759065, /* d0[2] */
    XFIX_2_613125930  /* d0[3] */
  };

  quantptr = (IFAST_MULT_TYPE *) compptr->dct_table;
  inptr = coef_block;
  asm volatile (
    /* load constants */
    "vld1.16 {d0}, [%[c]]\n"
    /* load all coef block:
     *   0 | d4  d5
     *   1 | d6  d7
     *   2 | d8  d9
     *   3 | d10 d11
     *   4 | d12 d13
     *   5 | d14 d15
     *   6 | d16 d17
     *   7 | d18 d19
     */
    "vld1.16 {d4, d5, d6, d7}, [%[inptr]]!\n"
    "vld1.16 {d8, d9, d10, d11}, [%[inptr]]!\n"
    "vld1.16 {d12, d13, d14, d15}, [%[inptr]]!\n"
    "vld1.16 {d16, d17, d18, d19}, [%[inptr]]!\n"
    /* dequantize */
    "vld1.16 {d20, d21, d22, d23}, [%[quantptr]]!\n"
    "vmul.s16 q2, q2, q10\n"
    "vld1.16 {d24, d25, d26, d27}, [%[quantptr]]!\n"
    "vmul.s16 q3, q3, q11\n"
    "vmul.s16 q4, q4, q12\n"
    "vld1.16 {d28, d29, d30, d31}, [%[quantptr]]!\n"
    "vmul.s16 q5, q5, q13\n"
    "vmul.s16 q6, q6, q14\n"
    "vld1.16 {d20, d21, d22, d23}, [%[quantptr]]!\n"
    "vmul.s16 q7, q7, q15\n"
    "vmul.s16 q8, q8, q10\n"
    "vmul.s16 q9, q9, q11\n"

    ".macro idct_helper x0, x1, x2, x3, x4, x5, x6, x7,"
    "                   t10, t11, t12, t13, t14\n"
    "vsub.s16     \\t10, \\x0, \\x4\n"
    "vadd.s16     \\x4,  \\x0, \\x4\n"
    "vswp.s16     \\t10, \\x0\n"
    "vsub.s16     \\t11, \\x2, \\x6\n"
    "vadd.s16     \\x6,  \\x2, \\x6\n"
    "vswp.s16     \\t11, \\x2\n"
    "vsub.s16     \\t10, \\x3, \\x5\n"
    "vadd.s16     \\x5,  \\x3, \\x5\n"
    "vswp.s16     \\t10, \\x3\n"
    "vsub.s16     \\t11, \\x1, \\x7\n"
    "vadd.s16     \\x7,  \\x1, \\x7\n"
    "vswp.s16     \\t11, \\x1\n"

    "vqdmulh.s16  \\t13, \\x2,  d0[1]\n"
    "vadd.s16     \\t12, \\x3,  \\x3\n"
    "vadd.s16     \\x2,  \\x2,  \\t13\n"
    "vqdmulh.s16  \\t13, \\x3,  d0[3]\n"
    "vsub.s16     \\t10,  \\x1, \\x3\n"
    "vadd.s16     \\t12, \\t12, \\t13\n"
    "vqdmulh.s16  \\t13, \\t10, d0[2]\n"
    "vsub.s16     \\t11, \\x7,  \\x5\n"
    "vadd.s16     \\t10, \\t10, \\t13\n"
    "vqdmulh.s16  \\t13, \\t11, d0[1]\n"
    "vadd.s16     \\t11, \\t11, \\t13\n"

    "vqdmulh.s16  \\t13, \\x1,  d0[0]\n"
    "vsub.s16     \\x2,  \\x6,  \\x2\n"
    "vsub.s16     \\t14, \\x0,  \\x2\n"
    "vadd.s16     \\x2,  \\x0,  \\x2\n"
    "vadd.s16     \\x0,  \\x4,  \\x6\n"
    "vsub.s16     \\x4,  \\x4,  \\x6\n"
    "vadd.s16     \\x1,  \\x1,  \\t13\n"
    "vadd.s16     \\t13, \\x7,  \\x5\n"
    "vsub.s16     \\t12, \\t13, \\t12\n"
    "vsub.s16     \\t12, \\t12, \\t10\n"
    "vadd.s16     \\t11, \\t12, \\t11\n"
    "vsub.s16     \\t10, \\x1,  \\t10\n"
    "vadd.s16     \\t10, \\t10, \\t11\n"

    "vsub.s16     \\x7,  \\x0,  \\t13\n"
    "vadd.s16     \\x0,  \\x0,  \\t13\n"
    "vadd.s16     \\x6,  \\t14, \\t12\n"
    "vsub.s16     \\x1,  \\t14, \\t12\n"
    "vsub.s16     \\x5,  \\x2,  \\t11\n"
    "vadd.s16     \\x2,  \\x2,  \\t11\n"
    "vsub.s16     \\x3,  \\x4,  \\t10\n"
    "vadd.s16     \\x4,  \\x4,  \\t10\n"
    ".endm\n"

    ".macro transpose_4x4 x0, x1, x2, x3\n"
    "vtrn.16 \\x0, \\x1\n"
    "vtrn.16 \\x2, \\x3\n"
    "vtrn.32 \\x0, \\x2\n"
    "vtrn.32 \\x1, \\x3\n"
    ".endm\n"

    /* pass 1 */
    "idct_helper q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14\n"
    /* transpose */
    "transpose_4x4  d4,  d6,  d8,  d10\n"
    "transpose_4x4  d5,  d7,  d9,  d11\n"
    "transpose_4x4  d12, d14, d16, d18\n"
    "transpose_4x4  d13, d15, d17, d19\n"
    "vswp           d12, d5\n"
    "vswp           d14, d7\n"
    "vswp           d16, d9\n"
    "vswp           d18, d11\n"

    /* pass2 */
    "idct_helper q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14\n"
    /* transpose */
    "transpose_4x4  d4,  d6,  d8,  d10\n"
    "transpose_4x4  d5,  d7,  d9,  d11\n"
    "transpose_4x4  d12, d14, d16, d18\n"
    "transpose_4x4  d13, d15, d17, d19\n"
    "vswp           d12, d5\n"
    "vswp           d14, d7\n"
    "vswp           d16, d9\n"
    "vswp           d18, d11\n"

    /* descale and range limit */
    "vmov.s16       q15, #(0x80 << 5)\n"
    "vqadd.s16      q2, q2, q15\n"
    "vqadd.s16      q3, q3, q15\n"
    "vqadd.s16      q4, q4, q15\n"
    "vqadd.s16      q5, q5, q15\n"
    "vqadd.s16      q6, q6, q15\n"
    "vqadd.s16      q7, q7, q15\n"
    "vqadd.s16      q8, q8, q15\n"
    "vqadd.s16      q9, q9, q15\n"
    "vqshrun.s16    d4, q2, #5\n"
    "vqshrun.s16    d6, q3, #5\n"
    "vqshrun.s16    d8, q4, #5\n"
    "vqshrun.s16    d10, q5, #5\n"
    "vqshrun.s16    d12, q6, #5\n"
    "vqshrun.s16    d14, q7, #5\n"
    "vqshrun.s16    d16, q8, #5\n"
    "vqshrun.s16    d18, q9, #5\n"

    /* store results to the output buffer */
    ".irp x, d4, d6, d8, d10, d12, d14, d16, d18\n"
    "ldr            %[tmp], [%[output_buf]], #4\n"
    "add            %[tmp], %[tmp], %[output_col]\n"
    "vst1.8         {\\x}, [%[tmp]]!\n"
    ".endr\n"
    : [inptr] "+&r" (inptr),
      [quantptr] "+&r" (quantptr),
      [tmp] "=&r" (tmp),
      [output_buf] "+&r" (output_buf)
    : [c] "r" (c),
      [output_col] "r" (output_col)
    : "cc", "memory",
      "d0",  "d1",  "d2",  "d3",  "d4",  "d5",  "d6",  "d7",
      "d8",  "d9",  "d10", "d11", "d12", "d13", "d14", "d15",
      "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23",
      "d24", "d25", "d26", "d27", "d28", "d29", "d30", "d31");
}

#if 0

/* Macro which is similar to VQDMULH NEON instruction */
#define XMULTIPLY(var,const)  ((DCTELEM) DESCALE((var) * (const) * 2, 16))

/*
 * A slightly modified C code (which maps to NEON instructions better),
 * which was used as a reference implementation for converting to NEON.
 */
GLOBAL(void)
jpeg_idct_ifast (j_decompress_ptr cinfo, jpeg_component_info * compptr,
		 JCOEFPTR coef_block,
		 JSAMPARRAY output_buf, JDIMENSION output_col)
{
  DCTELEM q10, q11, q12, q13, q14;
  short * inptr;
  IFAST_MULT_TYPE * quantptr;
  short * wsptr;
  JSAMPROW outptr;
  JSAMPLE *range_limit = IDCT_range_limit(cinfo);
  int ctr;
  short workspace[DCTSIZE2];	/* buffers data between passes */
  SHIFT_TEMPS			/* for DESCALE */
  ISHIFT_TEMPS			/* for IDESCALE */
  JCOEF dequantized_input[DCTSIZE*8];

  /* Pass 0: dequantize data */
  quantptr = (IFAST_MULT_TYPE *) compptr->dct_table;
  inptr = coef_block;
  for (ctr = 0; ctr < 64; ctr++)
    dequantized_input[ctr] = DEQUANTIZE(inptr[ctr], quantptr[ctr]);

  /* Pass 1: process columns from input, store into work array. */

  /* preprocess input data, converting them to sums and differences */
  inptr = dequantized_input;
  for (ctr = 0; ctr < 8; ctr++) {
    int sum  = inptr[ctr + DCTSIZE*0] + inptr[ctr + DCTSIZE*4];
    int diff = inptr[ctr + DCTSIZE*0] - inptr[ctr + DCTSIZE*4];
    inptr[ctr + DCTSIZE*0] = diff;
    inptr[ctr + DCTSIZE*4] = sum;
    sum  = inptr[ctr + DCTSIZE*2] + inptr[ctr + DCTSIZE*6];
    diff = inptr[ctr + DCTSIZE*2] - inptr[ctr + DCTSIZE*6];
    inptr[ctr + DCTSIZE*2] = diff;
    inptr[ctr + DCTSIZE*6] = sum;
    sum  = inptr[ctr + DCTSIZE*3] + inptr[ctr + DCTSIZE*5];
    diff = inptr[ctr + DCTSIZE*3] - inptr[ctr + DCTSIZE*5];
    inptr[ctr + DCTSIZE*3] = diff;
    inptr[ctr + DCTSIZE*5] = sum;
    sum  = inptr[ctr + DCTSIZE*1] + inptr[ctr + DCTSIZE*7];
    diff = inptr[ctr + DCTSIZE*1] - inptr[ctr + DCTSIZE*7];
    inptr[ctr + DCTSIZE*1] = diff;
    inptr[ctr + DCTSIZE*7] = sum;
  }
  wsptr = workspace;
  for (ctr = DCTSIZE; ctr > 0; ctr--) {

    q13 = XMULTIPLY(inptr[DCTSIZE*2], XFIX_1_414213562);
    q12 = inptr[DCTSIZE*3] + inptr[DCTSIZE*3];
    inptr[DCTSIZE*2] += q13;
    q13 = XMULTIPLY(inptr[DCTSIZE*3], XFIX_2_613125930);
    q10 = inptr[DCTSIZE*1] - inptr[DCTSIZE*3];
    q12 += q13;
    q13 = XMULTIPLY(q10, XFIX_1_847759065);
    q11 = inptr[DCTSIZE*7] - inptr[DCTSIZE*5];
    q10 += q13;
    q13 = XMULTIPLY(q11, XFIX_1_414213562);
    q11 += q13;

    q13 = XMULTIPLY(inptr[DCTSIZE*1], XFIX_1_082392200);

    inptr[DCTSIZE*2] = inptr[DCTSIZE*6] - inptr[DCTSIZE*2];
    q14 = inptr[DCTSIZE*0] - inptr[DCTSIZE*2];
    inptr[DCTSIZE*2] = inptr[DCTSIZE*0] + inptr[DCTSIZE*2];
    inptr[DCTSIZE*0] = inptr[DCTSIZE*4] + inptr[DCTSIZE*6];
    inptr[DCTSIZE*4] = inptr[DCTSIZE*4] - inptr[DCTSIZE*6];

    inptr[DCTSIZE*1] += q13;

    q13 = inptr[DCTSIZE*7] + inptr[DCTSIZE*5];
    q12 = q13 - q12 - q10;
    q11 = q12 + q11;
    q10 = q11 + inptr[DCTSIZE*1] - q10;

    wsptr[7] = (int) (inptr[DCTSIZE*0] - q13);
    wsptr[0] = (int) (inptr[DCTSIZE*0] + q13);
    wsptr[6] = (int) (q14 + q12);
    wsptr[1] = (int) (q14 - q12);
    wsptr[5] = (int) (inptr[DCTSIZE*2] - q11);
    wsptr[2] = (int) (inptr[DCTSIZE*2] + q11);
    wsptr[3] = (int) (inptr[DCTSIZE*4] - q10);
    wsptr[4] = (int) (inptr[DCTSIZE*4] + q10);

    inptr++;			/* advance pointers to next column */
    wsptr += DCTSIZE;
  }

  /* Pass 2: process rows from work array, store into output array. */
  /* Note that we must descale the results by a factor of 8 == 2**3, */
  /* and also undo the PASS1_BITS scaling. */
  inptr = workspace;
  for (ctr = 0; ctr < 8; ctr++) {
    int sum  = inptr[ctr + DCTSIZE*0] + inptr[ctr + DCTSIZE*4];
    int diff = inptr[ctr + DCTSIZE*0] - inptr[ctr + DCTSIZE*4];
    inptr[ctr + DCTSIZE*0] = diff;
    inptr[ctr + DCTSIZE*4] = sum;
    sum  = inptr[ctr + DCTSIZE*2] + inptr[ctr + DCTSIZE*6];
    diff = inptr[ctr + DCTSIZE*2] - inptr[ctr + DCTSIZE*6];
    inptr[ctr + DCTSIZE*2] = diff;
    inptr[ctr + DCTSIZE*6] = sum;
    sum  = inptr[ctr + DCTSIZE*3] + inptr[ctr + DCTSIZE*5];
    diff = inptr[ctr + DCTSIZE*3] - inptr[ctr + DCTSIZE*5];
    inptr[ctr + DCTSIZE*3] = diff;
    inptr[ctr + DCTSIZE*5] = sum;
    sum  = inptr[ctr + DCTSIZE*1] + inptr[ctr + DCTSIZE*7];
    diff = inptr[ctr + DCTSIZE*1] - inptr[ctr + DCTSIZE*7];
    inptr[ctr + DCTSIZE*1] = diff;
    inptr[ctr + DCTSIZE*7] = sum;
  }

  for (ctr = 0; ctr < DCTSIZE; ctr++) {
    outptr = output_buf[ctr] + output_col;

    q13 = XMULTIPLY(inptr[DCTSIZE*2], XFIX_1_414213562);
    q12 = inptr[DCTSIZE*3] + inptr[DCTSIZE*3];
    inptr[DCTSIZE*2] += q13;
    q13 = XMULTIPLY(inptr[DCTSIZE*3], XFIX_2_613125930);
    q10 = inptr[DCTSIZE*1] - inptr[DCTSIZE*3];
    q12 += q13;
    q13 = XMULTIPLY(q10, XFIX_1_847759065);
    q11 = inptr[DCTSIZE*7] - inptr[DCTSIZE*5];
    q10 += q13;
    q13 = XMULTIPLY(q11, XFIX_1_414213562);
    q11 += q13;

    q13 = XMULTIPLY(inptr[DCTSIZE*1], XFIX_1_082392200);

    inptr[DCTSIZE*2] = inptr[DCTSIZE*6] - inptr[DCTSIZE*2];
    q14 = inptr[DCTSIZE*0] - inptr[DCTSIZE*2];
    inptr[DCTSIZE*2] = inptr[DCTSIZE*0] + inptr[DCTSIZE*2];
    inptr[DCTSIZE*0] = inptr[DCTSIZE*4] + inptr[DCTSIZE*6];
    inptr[DCTSIZE*4] = inptr[DCTSIZE*4] - inptr[DCTSIZE*6];

    inptr[DCTSIZE*1] += q13;

    q13 = inptr[DCTSIZE*7] + inptr[DCTSIZE*5];
    q12 = q13 - q12 - q10;
    q11 = q12 + q11;
    q10 = q11 + inptr[DCTSIZE*1] - q10;

    /* Final output stage: scale down by a factor of 8 and range-limit */
    outptr[7] = range_limit[IDESCALE(inptr[DCTSIZE*0] - q13, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[0] = range_limit[IDESCALE(inptr[DCTSIZE*0] + q13, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[6] = range_limit[IDESCALE(q14 + q12, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[1] = range_limit[IDESCALE(q14 - q12, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[5] = range_limit[IDESCALE(inptr[DCTSIZE*2] - q11, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[2] = range_limit[IDESCALE(inptr[DCTSIZE*2] + q11, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[3] = range_limit[IDESCALE(inptr[DCTSIZE*4] - q10, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[4] = range_limit[IDESCALE(inptr[DCTSIZE*4] + q10, PASS1_BITS+3)
			    & RANGE_MASK];

    inptr++;			/* advance pointers to next column */
  }
}

#endif

#else

GLOBAL(void)
jpeg_idct_ifast (j_decompress_ptr cinfo, jpeg_component_info * compptr,
		 JCOEFPTR coef_block,
		 JSAMPARRAY output_buf, JDIMENSION output_col)
{
  DCTELEM tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7;
  DCTELEM tmp10, tmp11, tmp12, tmp13;
  DCTELEM z5, z10, z11, z12, z13;
  JCOEFPTR inptr;
  IFAST_MULT_TYPE * quantptr;
  int * wsptr;
  JSAMPROW outptr;
  JSAMPLE *range_limit = IDCT_range_limit(cinfo);
  int ctr;
  int workspace[DCTSIZE2];	/* buffers data between passes */
  SHIFT_TEMPS			/* for DESCALE */
  ISHIFT_TEMPS			/* for IDESCALE */

  /* Pass 1: process columns from input, store into work array. */

  inptr = coef_block;
  quantptr = (IFAST_MULT_TYPE *) compptr->dct_table;
  wsptr = workspace;
  for (ctr = DCTSIZE; ctr > 0; ctr--) {
    /* Due to quantization, we will usually find that many of the input
     * coefficients are zero, especially the AC terms.  We can exploit this
     * by short-circuiting the IDCT calculation for any column in which all
     * the AC terms are zero.  In that case each output is equal to the
     * DC coefficient (with scale factor as needed).
     * With typical images and quantization tables, half or more of the
     * column DCT calculations can be simplified this way.
     */
    
    if (inptr[DCTSIZE*1] == 0 && inptr[DCTSIZE*2] == 0 &&
	inptr[DCTSIZE*3] == 0 && inptr[DCTSIZE*4] == 0 &&
	inptr[DCTSIZE*5] == 0 && inptr[DCTSIZE*6] == 0 &&
	inptr[DCTSIZE*7] == 0) {
      /* AC terms all zero */
      int dcval = (int) DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]);

      wsptr[DCTSIZE*0] = dcval;
      wsptr[DCTSIZE*1] = dcval;
      wsptr[DCTSIZE*2] = dcval;
      wsptr[DCTSIZE*3] = dcval;
      wsptr[DCTSIZE*4] = dcval;
      wsptr[DCTSIZE*5] = dcval;
      wsptr[DCTSIZE*6] = dcval;
      wsptr[DCTSIZE*7] = dcval;
      
      inptr++;			/* advance pointers to next column */
      quantptr++;
      wsptr++;
      continue;
    }
    
    /* Even part */

    tmp0 = DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]);
    tmp1 = DEQUANTIZE(inptr[DCTSIZE*2], quantptr[DCTSIZE*2]);
    tmp2 = DEQUANTIZE(inptr[DCTSIZE*4], quantptr[DCTSIZE*4]);
    tmp3 = DEQUANTIZE(inptr[DCTSIZE*6], quantptr[DCTSIZE*6]);

    tmp10 = tmp0 + tmp2;	/* phase 3 */
    tmp11 = tmp0 - tmp2;

    tmp13 = tmp1 + tmp3;	/* phases 5-3 */
    tmp12 = MULTIPLY(tmp1 - tmp3, FIX_1_414213562) - tmp13; /* 2*c4 */

    tmp0 = tmp10 + tmp13;	/* phase 2 */
    tmp3 = tmp10 - tmp13;
    tmp1 = tmp11 + tmp12;
    tmp2 = tmp11 - tmp12;
    
    /* Odd part */

    tmp4 = DEQUANTIZE(inptr[DCTSIZE*1], quantptr[DCTSIZE*1]);
    tmp5 = DEQUANTIZE(inptr[DCTSIZE*3], quantptr[DCTSIZE*3]);
    tmp6 = DEQUANTIZE(inptr[DCTSIZE*5], quantptr[DCTSIZE*5]);
    tmp7 = DEQUANTIZE(inptr[DCTSIZE*7], quantptr[DCTSIZE*7]);

    z13 = tmp6 + tmp5;		/* phase 6 */
    z10 = tmp6 - tmp5;
    z11 = tmp4 + tmp7;
    z12 = tmp4 - tmp7;

    tmp7 = z11 + z13;		/* phase 5 */
    tmp11 = MULTIPLY(z11 - z13, FIX_1_414213562); /* 2*c4 */

    z5 = MULTIPLY(z10 + z12, FIX_1_847759065); /* 2*c2 */
    tmp10 = MULTIPLY(z12, FIX_1_082392200) - z5; /* 2*(c2-c6) */
    tmp12 = MULTIPLY(z10, - FIX_2_613125930) + z5; /* -2*(c2+c6) */

    tmp6 = tmp12 - tmp7;	/* phase 2 */
    tmp5 = tmp11 - tmp6;
    tmp4 = tmp10 + tmp5;

    wsptr[DCTSIZE*0] = (int) (tmp0 + tmp7);
    wsptr[DCTSIZE*7] = (int) (tmp0 - tmp7);
    wsptr[DCTSIZE*1] = (int) (tmp1 + tmp6);
    wsptr[DCTSIZE*6] = (int) (tmp1 - tmp6);
    wsptr[DCTSIZE*2] = (int) (tmp2 + tmp5);
    wsptr[DCTSIZE*5] = (int) (tmp2 - tmp5);
    wsptr[DCTSIZE*4] = (int) (tmp3 + tmp4);
    wsptr[DCTSIZE*3] = (int) (tmp3 - tmp4);

    inptr++;			/* advance pointers to next column */
    quantptr++;
    wsptr++;
  }
  
  /* Pass 2: process rows from work array, store into output array. */
  /* Note that we must descale the results by a factor of 8 == 2**3, */
  /* and also undo the PASS1_BITS scaling. */

  wsptr = workspace;
  for (ctr = 0; ctr < DCTSIZE; ctr++) {
    outptr = output_buf[ctr] + output_col;
    /* Rows of zeroes can be exploited in the same way as we did with columns.
     * However, the column calculation has created many nonzero AC terms, so
     * the simplification applies less often (typically 5% to 10% of the time).
     * On machines with very fast multiplication, it's possible that the
     * test takes more time than it's worth.  In that case this section
     * may be commented out.
     */
    
#ifndef NO_ZERO_ROW_TEST
    if (wsptr[1] == 0 && wsptr[2] == 0 && wsptr[3] == 0 && wsptr[4] == 0 &&
	wsptr[5] == 0 && wsptr[6] == 0 && wsptr[7] == 0) {
      /* AC terms all zero */
      JSAMPLE dcval = range_limit[IDESCALE(wsptr[0], PASS1_BITS+3)
				  & RANGE_MASK];
      
      outptr[0] = dcval;
      outptr[1] = dcval;
      outptr[2] = dcval;
      outptr[3] = dcval;
      outptr[4] = dcval;
      outptr[5] = dcval;
      outptr[6] = dcval;
      outptr[7] = dcval;

      wsptr += DCTSIZE;		/* advance pointer to next row */
      continue;
    }
#endif
    
    /* Even part */

    tmp10 = ((DCTELEM) wsptr[0] + (DCTELEM) wsptr[4]);
    tmp11 = ((DCTELEM) wsptr[0] - (DCTELEM) wsptr[4]);

    tmp13 = ((DCTELEM) wsptr[2] + (DCTELEM) wsptr[6]);
    tmp12 = MULTIPLY((DCTELEM) wsptr[2] - (DCTELEM) wsptr[6], FIX_1_414213562)
	    - tmp13;

    tmp0 = tmp10 + tmp13;
    tmp3 = tmp10 - tmp13;
    tmp1 = tmp11 + tmp12;
    tmp2 = tmp11 - tmp12;

    /* Odd part */

    z13 = (DCTELEM) wsptr[5] + (DCTELEM) wsptr[3];
    z10 = (DCTELEM) wsptr[5] - (DCTELEM) wsptr[3];
    z11 = (DCTELEM) wsptr[1] + (DCTELEM) wsptr[7];
    z12 = (DCTELEM) wsptr[1] - (DCTELEM) wsptr[7];

    tmp7 = z11 + z13;		/* phase 5 */
    tmp11 = MULTIPLY(z11 - z13, FIX_1_414213562); /* 2*c4 */

    z5 = MULTIPLY(z10 + z12, FIX_1_847759065); /* 2*c2 */
    tmp10 = MULTIPLY(z12, FIX_1_082392200) - z5; /* 2*(c2-c6) */
    tmp12 = MULTIPLY(z10, - FIX_2_613125930) + z5; /* -2*(c2+c6) */

    tmp6 = tmp12 - tmp7;	/* phase 2 */
    tmp5 = tmp11 - tmp6;
    tmp4 = tmp10 + tmp5;

    /* Final output stage: scale down by a factor of 8 and range-limit */

    outptr[0] = range_limit[IDESCALE(tmp0 + tmp7, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[7] = range_limit[IDESCALE(tmp0 - tmp7, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[1] = range_limit[IDESCALE(tmp1 + tmp6, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[6] = range_limit[IDESCALE(tmp1 - tmp6, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[2] = range_limit[IDESCALE(tmp2 + tmp5, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[5] = range_limit[IDESCALE(tmp2 - tmp5, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[4] = range_limit[IDESCALE(tmp3 + tmp4, PASS1_BITS+3)
			    & RANGE_MASK];
    outptr[3] = range_limit[IDESCALE(tmp3 - tmp4, PASS1_BITS+3)
			    & RANGE_MASK];

    wsptr += DCTSIZE;		/* advance pointer to next row */
  }
}

#endif

#endif /* DCT_IFAST_SUPPORTED */
