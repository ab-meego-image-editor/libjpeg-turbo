/*
 * jidctred.c
 *
 * Copyright (C) 1994-1998, Thomas G. Lane.
 *
 * ARM NEON optimizations
 * Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.
 * Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
 *
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file contains inverse-DCT routines that produce reduced-size output:
 * either 4x4, 2x2, or 1x1 pixels from an 8x8 DCT block.
 *
 * The implementation is based on the Loeffler, Ligtenberg and Moschytz (LL&M)
 * algorithm used in jidctint.c.  We simply replace each 8-to-8 1-D IDCT step
 * with an 8-to-4 step that produces the four averages of two adjacent outputs
 * (or an 8-to-2 step producing two averages of four outputs, for 2x2 output).
 * These steps were derived by computing the corresponding values at the end
 * of the normal LL&M code, then simplifying as much as possible.
 *
 * 1x1 is trivial: just take the DC coefficient divided by 8.
 *
 * See jidctint.c for additional comments.
 */

#define JPEG_INTERNALS
#include "jinclude.h"
#include "jpeglib.h"
#include "jdct.h"		/* Private declarations for DCT subsystem */

#ifdef IDCT_SCALING_SUPPORTED


/*
 * This module is specialized to the case DCTSIZE = 8.
 */

#if DCTSIZE != 8
  Sorry, this code only copes with 8x8 DCTs. /* deliberate syntax err */
#endif


/* Scaling is the same as in jidctint.c. */

#if BITS_IN_JSAMPLE == 8
#define CONST_BITS  13
#define PASS1_BITS  2
#else
#define CONST_BITS  13
#define PASS1_BITS  1		/* lose a little precision to avoid overflow */
#endif

/* Some C compilers fail to reduce "FIX(constant)" at compile time, thus
 * causing a lot of useless floating-point operations at run time.
 * To get around this we use the following pre-calculated constants.
 * If you change CONST_BITS you may want to add appropriate values.
 * (With a reasonable C compiler, you can just rely on the FIX() macro...)
 */

#if CONST_BITS == 13
#define FIX_0_211164243  ((INT32)  1730)	/* FIX(0.211164243) */
#define FIX_0_509795579  ((INT32)  4176)	/* FIX(0.509795579) */
#define FIX_0_601344887  ((INT32)  4926)	/* FIX(0.601344887) */
#define FIX_0_720959822  ((INT32)  5906)	/* FIX(0.720959822) */
#define FIX_0_765366865  ((INT32)  6270)	/* FIX(0.765366865) */
#define FIX_0_850430095  ((INT32)  6967)	/* FIX(0.850430095) */
#define FIX_0_899976223  ((INT32)  7373)	/* FIX(0.899976223) */
#define FIX_1_061594337  ((INT32)  8697)	/* FIX(1.061594337) */
#define FIX_1_272758580  ((INT32)  10426)	/* FIX(1.272758580) */
#define FIX_1_451774981  ((INT32)  11893)	/* FIX(1.451774981) */
#define FIX_1_847759065  ((INT32)  15137)	/* FIX(1.847759065) */
#define FIX_2_172734803  ((INT32)  17799)	/* FIX(2.172734803) */
#define FIX_2_562915447  ((INT32)  20995)	/* FIX(2.562915447) */
#define FIX_3_624509785  ((INT32)  29692)	/* FIX(3.624509785) */
#else
#define FIX_0_211164243  FIX(0.211164243)
#define FIX_0_509795579  FIX(0.509795579)
#define FIX_0_601344887  FIX(0.601344887)
#define FIX_0_720959822  FIX(0.720959822)
#define FIX_0_765366865  FIX(0.765366865)
#define FIX_0_850430095  FIX(0.850430095)
#define FIX_0_899976223  FIX(0.899976223)
#define FIX_1_061594337  FIX(1.061594337)
#define FIX_1_272758580  FIX(1.272758580)
#define FIX_1_451774981  FIX(1.451774981)
#define FIX_1_847759065  FIX(1.847759065)
#define FIX_2_172734803  FIX(2.172734803)
#define FIX_2_562915447  FIX(2.562915447)
#define FIX_3_624509785  FIX(3.624509785)
#endif


/* Multiply an INT32 variable by an INT32 constant to yield an INT32 result.
 * For 8-bit samples with the recommended scaling, all the variable
 * and constant values involved are no more than 16 bits wide, so a
 * 16x16->32 bit multiply can be used instead of a full 32x32 multiply.
 * For 12-bit samples, a full 32-bit multiplication will be needed.
 */

#if BITS_IN_JSAMPLE == 8
#define MULTIPLY(var,const)  MULTIPLY16C16(var,const)
#else
#define MULTIPLY(var,const)  ((var) * (const))
#endif


/* Dequantize a coefficient by multiplying it by the multiplier-table
 * entry; produce an int result.  In this module, both inputs and result
 * are 16 bits or less, so either int or short multiply will work.
 */

#define DEQUANTIZE(coef,quantval)  (((ISLOW_MULT_TYPE) (coef)) * (quantval))


/*
 * Perform dequantization and inverse DCT on one block of coefficients,
 * producing a reduced-size 4x4 output block.
 */

#if defined(WITH_SIMD) && defined(__ARM_NEON__) && (BITS_IN_JSAMPLE == 8)

/* ARM NEON optimized version of 'jpeg_idct_4x4' */
GLOBAL(void)
jpeg_idct_4x4_neon (j_decompress_ptr cinfo, jpeg_component_info * compptr,
		    JCOEFPTR coef_block,
		    JSAMPARRAY output_buf, JDIMENSION output_col)
{
  JCOEFPTR inptr = coef_block;
  ISLOW_MULT_TYPE * quantptr = compptr->dct_table;
  int tmp;

  const static short c[12] = {
    FIX_1_847759065,     /* d0[0] */
    -FIX_0_765366865,    /* d0[1] */
    -FIX_0_211164243,    /* d0[2] */
    FIX_1_451774981,     /* d0[3] */
    -FIX_2_172734803,    /* d1[0] */
    FIX_1_061594337,     /* d1[1] */
    -FIX_0_509795579,    /* d1[2] */
    -FIX_0_601344887,    /* d1[3] */
    FIX_0_899976223,     /* d2[0] */
    FIX_2_562915447,     /* d2[1] */
    1 << (CONST_BITS+1), /* d2[2] */
    0};                  /* d2[3] */

  asm volatile (
    /* load constants */
    "vld1.16 {d0, d1, d2}, [%[c]]\n"
    /* load all coef block:
     *   0 | d4  d5
     *   1 | d6  d7
     *   2 | d8  d9
     *   3 | d10 d11
     *   4 |
     *   5 | d12 d13
     *   6 | d14 d15
     *   7 | d16 d17
     */
    "vld1.16 {d4, d5, d6, d7}, [%[inptr]]!\n"
    "vld1.16 {d8, d9, d10, d11}, [%[inptr]]!\n"
    "add %[inptr], %[inptr], #16\n"
    "vld1.16 {d12, d13, d14, d15}, [%[inptr]]!\n"
    "vld1.16 {d16, d17}, [%[inptr]]!\n"
    /* dequantize */
    "vld1.16 {d18, d19, d20, d21}, [%[quantptr]]!\n"
    "vmul.s16 q2, q2, q9\n"
    "vld1.16 {d22, d23, d24, d25}, [%[quantptr]]!\n"
    "vmul.s16 q3, q3, q10\n"
    "vmul.s16 q4, q4, q11\n"
    "add %[quantptr], %[quantptr], #16\n"
    "vld1.16 {d26, d27, d28, d29}, [%[quantptr]]!\n"
    "vmul.s16 q5, q5, q12\n"
    "vmul.s16 q6, q6, q13\n"
    "vld1.16 {d30, d31}, [%[quantptr]]!\n"
    "vmul.s16 q7, q7, q14\n"
    "vmul.s16 q8, q8, q15\n"
    /*
     * tmp0  : q12
     * tmp2  : q13
     * tmp10 : q14
     * tmp12 : q15
     */
     ".macro idct_helper x4, x6, x8, x10, x12, x14, x16, shift,"
     "                   y26, y27, y28, y29\n"
     "vmull.s16 q14, \\x4, d2[2]\n"
     "vmlal.s16 q14, \\x8, d0[0]\n"
     "vmlal.s16 q14, \\x14, d0[1]\n"

     "vmull.s16 q13, \\x16, d1[2]\n"
     "vmlal.s16 q13, \\x12, d1[3]\n"
     "vmlal.s16 q13, \\x10, d2[0]\n"
     "vmlal.s16 q13, \\x6, d2[1]\n"

     "vmull.s16 q15, \\x4, d2[2]\n"
     "vmlsl.s16 q15, \\x8, d0[0]\n"
     "vmlsl.s16 q15, \\x14, d0[1]\n"

     "vmull.s16 q12, \\x16, d0[2]\n"
     "vmlal.s16 q12, \\x12, d0[3]\n"
     "vmlal.s16 q12, \\x10, d1[0]\n"
     "vmlal.s16 q12, \\x6, d1[1]\n"

     "vadd.s32  q10, q14, q13\n"
     "vsub.s32  q14, q14, q13\n"
     ".if \\shift > 16\n"
     "  vrshr.s32 q10, q10, #\\shift\n"
     "  vrshr.s32 q14, q14, #\\shift\n"
     "  vmovn.s32 \\y26, q10\n"
     "  vmovn.s32 \\y27, q14\n"
     ".else\n"
     "  vrshrn.s32 \\y26, q10, #\\shift\n"
     "  vrshrn.s32 \\y27, q14, #\\shift\n"
     ".endif\n"
     "vadd.s32  q10, q15, q12\n"
     "vsub.s32  q15, q15, q12\n"
     ".if \\shift > 16\n"
     "  vrshr.s32 q10, q10, #\\shift\n"
     "  vrshr.s32 q15, q15, #\\shift\n"
     "  vmovn.s32 \\y28, q10\n"
     "  vmovn.s32 \\y29, q15\n"
     ".else\n"
     "  vrshrn.s32 \\y28, q10, #\\shift\n"
     "  vrshrn.s32 \\y29, q15, #\\shift\n"
     ".endif\n"
     ".endm\n"
     /* do idct, transposing results after each step */
     /* pass 1 */
     "idct_helper d4, d6, d8, d10, d12, d14, d16, 12, d4, d6, d8, d10\n"
     "vtrn.16 d4, d8\n"
     "vtrn.16 d10, d6\n"
     "vtrn.32 d4, d10\n"
     "vtrn.32 d8, d6\n"
     "idct_helper d5, d7, d9, d11, d13, d15, d17, 12, d5, d7, d9, d11\n"
     "vtrn.16 d5, d9\n"
     "vtrn.16 d11, d7\n"
     "vtrn.32 d5, d11\n"
     "vtrn.32 d9, d7\n"
      /* pass 2 */
     "idct_helper d4, d8, d10, d6, d9, d11, d7, 19, d26, d27, d28, d29\n"
     "vtrn.16 d26, d28\n"
     "vtrn.16 d29, d27\n"
     "vtrn.32 d26, d29\n"
     "vtrn.32 d28, d27\n"
     /* range limit */
     "vmov.u16 q15, #0x80\n"
     "vadd.s16 q13, q13, q15\n"
     "vadd.s16 q14, q14, q15\n"
     "vqmovun.s16 d26, q13\n"
     "vqmovun.s16 d27, q14\n"
     /* store results to the output buffer */
     "ldr %[tmp], [%[output_buf]], #4\n"
     "add %[tmp], %[tmp], %[output_col]\n"
     "vst1.8 {d26[0]}, [%[tmp]]!\n"
     "vst1.8 {d26[1]}, [%[tmp]]!\n"
     "vst1.8 {d26[2]}, [%[tmp]]!\n"
     "vst1.8 {d26[3]}, [%[tmp]]!\n"

     "ldr %[tmp], [%[output_buf]], #4\n"
     "add %[tmp], %[tmp], %[output_col]\n"
     "vst1.8 {d27[0]}, [%[tmp]]!\n"
     "vst1.8 {d27[1]}, [%[tmp]]!\n"
     "vst1.8 {d27[2]}, [%[tmp]]!\n"
     "vst1.8 {d27[3]}, [%[tmp]]!\n"

     "ldr %[tmp], [%[output_buf]], #4\n"
     "add %[tmp], %[tmp], %[output_col]\n"
     "vst1.8 {d27[4]}, [%[tmp]]!\n"
     "vst1.8 {d27[5]}, [%[tmp]]!\n"
     "vst1.8 {d27[6]}, [%[tmp]]!\n"
     "vst1.8 {d27[7]}, [%[tmp]]!\n"

     "ldr %[tmp], [%[output_buf]], #4\n"
     "add %[tmp], %[tmp], %[output_col]\n"
     "vst1.8 {d26[4]}, [%[tmp]]!\n"
     "vst1.8 {d26[5]}, [%[tmp]]!\n"
     "vst1.8 {d26[6]}, [%[tmp]]!\n"
     "vst1.8 {d26[7]}, [%[tmp]]!\n"

    : [inptr] "+&r" (inptr),
      [quantptr] "+&r" (quantptr),
      [tmp] "=&r" (tmp),
      [output_buf] "+&r" (output_buf)
    : [c] "r" (c),
      [output_col] "r" (output_col)
    : "cc", "memory",
      "d0",  "d1",  "d2",  "d3",  "d4",  "d5",  "d6",  "d7",
      "d8",  "d9",  "d10", "d11", "d12", "d13", "d14", "d15",
      "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23",
      "d24", "d25", "d26", "d27", "d28", "d29", "d30", "d31");
}

#if 0

/*
 * A slightly modified C code (which maps to NEON instructions better),
 * which was used as a reference implementation for converting to NEON.
 */
GLOBAL(void)
jpeg_idct_4x4 (j_decompress_ptr cinfo, jpeg_component_info * compptr,
	       JCOEFPTR coef_block,
	       JSAMPARRAY output_buf, JDIMENSION output_col)
{
  INT32 tmp0, tmp2, tmp10, tmp12;
  INT32 z1, z2, z3, z4;
  JCOEFPTR inptr;
  ISLOW_MULT_TYPE * quantptr;
  short * wsptr;
  JSAMPROW outptr;
  JSAMPLE *range_limit = IDCT_range_limit(cinfo);
  int ctr;
  short workspace[DCTSIZE*8];	/* buffers data between passes */
  JCOEF dequantized_input[DCTSIZE*8];
  int i, tmp;
  SHIFT_TEMPS

  /* Pass 0: dequantize data. */
  quantptr = compptr->dct_table;
  inptr = coef_block;
  for (ctr = 0; ctr < 64; ctr++)
    dequantized_input[ctr] = DEQUANTIZE(inptr[ctr], quantptr[ctr]);

  /* Pass 1: process columns from input, store into work array. */
  inptr = dequantized_input;
  wsptr = workspace;
  for (ctr = DCTSIZE; ctr > 0; inptr++, wsptr+=DCTSIZE, ctr--) {

    /* Even part */
    tmp10 = (inptr[DCTSIZE*0] << (CONST_BITS+1))
          + MULTIPLY(inptr[DCTSIZE*2], FIX_1_847759065)
          + MULTIPLY(inptr[DCTSIZE*6], - FIX_0_765366865);
    tmp12 = (inptr[DCTSIZE*0] << (CONST_BITS+1))
          - MULTIPLY(inptr[DCTSIZE*2], FIX_1_847759065)
          - MULTIPLY(inptr[DCTSIZE*6], - FIX_0_765366865);

    /* Odd part */
    tmp0 = MULTIPLY(inptr[DCTSIZE*7], - FIX_0_211164243)
	 + MULTIPLY(inptr[DCTSIZE*5], FIX_1_451774981)
	 + MULTIPLY(inptr[DCTSIZE*3], - FIX_2_172734803)
	 + MULTIPLY(inptr[DCTSIZE*1], FIX_1_061594337);

    tmp2 = MULTIPLY(inptr[DCTSIZE*7], - FIX_0_509795579)
	 + MULTIPLY(inptr[DCTSIZE*5], - FIX_0_601344887)
	 + MULTIPLY(inptr[DCTSIZE*3], FIX_0_899976223)
	 + MULTIPLY(inptr[DCTSIZE*1], FIX_2_562915447);

    /* Final output stage */
    wsptr[0] = (int) DESCALE(tmp10 + tmp2, CONST_BITS-PASS1_BITS+1);
    wsptr[3] = (int) DESCALE(tmp10 - tmp2, CONST_BITS-PASS1_BITS+1);
    wsptr[1] = (int) DESCALE(tmp12 + tmp0, CONST_BITS-PASS1_BITS+1);
    wsptr[2] = (int) DESCALE(tmp12 - tmp0, CONST_BITS-PASS1_BITS+1);
  }

  /* Pass 2: process 4 rows from work array, store into output array. */
  inptr = workspace;
  for (ctr = 0; ctr < 4; ctr++, inptr++) {
    outptr = output_buf[ctr] + output_col;

    /* Even part */
    tmp10 = (inptr[DCTSIZE*0] << (CONST_BITS+1))
          + MULTIPLY(inptr[DCTSIZE*2], FIX_1_847759065)
          + MULTIPLY(inptr[DCTSIZE*6], - FIX_0_765366865);
    tmp12 = (inptr[DCTSIZE*0] << (CONST_BITS+1))
          - MULTIPLY(inptr[DCTSIZE*2], FIX_1_847759065)
          - MULTIPLY(inptr[DCTSIZE*6], - FIX_0_765366865);

    /* Odd part */
    tmp0 = MULTIPLY(inptr[DCTSIZE*7], - FIX_0_211164243)
	 + MULTIPLY(inptr[DCTSIZE*5], FIX_1_451774981)
	 + MULTIPLY(inptr[DCTSIZE*3], - FIX_2_172734803)
	 + MULTIPLY(inptr[DCTSIZE*1], FIX_1_061594337);

    tmp2 = MULTIPLY(inptr[DCTSIZE*7], - FIX_0_509795579)
	 + MULTIPLY(inptr[DCTSIZE*5], - FIX_0_601344887)
	 + MULTIPLY(inptr[DCTSIZE*3], FIX_0_899976223)
	 + MULTIPLY(inptr[DCTSIZE*1], FIX_2_562915447);

    /* Final output stage */
    outptr[0] = range_limit[(int) DESCALE(tmp10 + tmp2,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
    outptr[3] = range_limit[(int) DESCALE(tmp10 - tmp2,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
    outptr[1] = range_limit[(int) DESCALE(tmp12 + tmp0,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
    outptr[2] = range_limit[(int) DESCALE(tmp12 - tmp0,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
  }
}

#endif

#else

GLOBAL(void)
jpeg_idct_4x4 (j_decompress_ptr cinfo, jpeg_component_info * compptr,
	       JCOEFPTR coef_block,
	       JSAMPARRAY output_buf, JDIMENSION output_col)
{
  INT32 tmp0, tmp2, tmp10, tmp12;
  INT32 z1, z2, z3, z4;
  JCOEFPTR inptr;
  ISLOW_MULT_TYPE * quantptr;
  int * wsptr;
  JSAMPROW outptr;
  JSAMPLE *range_limit = IDCT_range_limit(cinfo);
  int ctr;
  int workspace[DCTSIZE*4];	/* buffers data between passes */
  SHIFT_TEMPS

  /* Pass 1: process columns from input, store into work array. */

  inptr = coef_block;
  quantptr = (ISLOW_MULT_TYPE *) compptr->dct_table;
  wsptr = workspace;
  for (ctr = DCTSIZE; ctr > 0; inptr++, quantptr++, wsptr++, ctr--) {
    /* Don't bother to process column 4, because second pass won't use it */
    if (ctr == DCTSIZE-4)
      continue;
    if (inptr[DCTSIZE*1] == 0 && inptr[DCTSIZE*2] == 0 &&
	inptr[DCTSIZE*3] == 0 && inptr[DCTSIZE*5] == 0 &&
	inptr[DCTSIZE*6] == 0 && inptr[DCTSIZE*7] == 0) {
      /* AC terms all zero; we need not examine term 4 for 4x4 output */
      int dcval = DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]) << PASS1_BITS;
      
      wsptr[DCTSIZE*0] = dcval;
      wsptr[DCTSIZE*1] = dcval;
      wsptr[DCTSIZE*2] = dcval;
      wsptr[DCTSIZE*3] = dcval;
      
      continue;
    }
    
    /* Even part */
    
    tmp0 = DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]);
    tmp0 <<= (CONST_BITS+1);
    
    z2 = DEQUANTIZE(inptr[DCTSIZE*2], quantptr[DCTSIZE*2]);
    z3 = DEQUANTIZE(inptr[DCTSIZE*6], quantptr[DCTSIZE*6]);

    tmp2 = MULTIPLY(z2, FIX_1_847759065) + MULTIPLY(z3, - FIX_0_765366865);
    
    tmp10 = tmp0 + tmp2;
    tmp12 = tmp0 - tmp2;
    
    /* Odd part */
    
    z1 = DEQUANTIZE(inptr[DCTSIZE*7], quantptr[DCTSIZE*7]);
    z2 = DEQUANTIZE(inptr[DCTSIZE*5], quantptr[DCTSIZE*5]);
    z3 = DEQUANTIZE(inptr[DCTSIZE*3], quantptr[DCTSIZE*3]);
    z4 = DEQUANTIZE(inptr[DCTSIZE*1], quantptr[DCTSIZE*1]);
    
    tmp0 = MULTIPLY(z1, - FIX_0_211164243) /* sqrt(2) * (c3-c1) */
	 + MULTIPLY(z2, FIX_1_451774981) /* sqrt(2) * (c3+c7) */
	 + MULTIPLY(z3, - FIX_2_172734803) /* sqrt(2) * (-c1-c5) */
	 + MULTIPLY(z4, FIX_1_061594337); /* sqrt(2) * (c5+c7) */
    
    tmp2 = MULTIPLY(z1, - FIX_0_509795579) /* sqrt(2) * (c7-c5) */
	 + MULTIPLY(z2, - FIX_0_601344887) /* sqrt(2) * (c5-c1) */
	 + MULTIPLY(z3, FIX_0_899976223) /* sqrt(2) * (c3-c7) */
	 + MULTIPLY(z4, FIX_2_562915447); /* sqrt(2) * (c1+c3) */

    /* Final output stage */
    
    wsptr[DCTSIZE*0] = (int) DESCALE(tmp10 + tmp2, CONST_BITS-PASS1_BITS+1);
    wsptr[DCTSIZE*3] = (int) DESCALE(tmp10 - tmp2, CONST_BITS-PASS1_BITS+1);
    wsptr[DCTSIZE*1] = (int) DESCALE(tmp12 + tmp0, CONST_BITS-PASS1_BITS+1);
    wsptr[DCTSIZE*2] = (int) DESCALE(tmp12 - tmp0, CONST_BITS-PASS1_BITS+1);
  }
  
  /* Pass 2: process 4 rows from work array, store into output array. */

  wsptr = workspace;
  for (ctr = 0; ctr < 4; ctr++) {
    outptr = output_buf[ctr] + output_col;
    /* It's not clear whether a zero row test is worthwhile here ... */

#ifndef NO_ZERO_ROW_TEST
    if (wsptr[1] == 0 && wsptr[2] == 0 && wsptr[3] == 0 &&
	wsptr[5] == 0 && wsptr[6] == 0 && wsptr[7] == 0) {
      /* AC terms all zero */
      JSAMPLE dcval = range_limit[(int) DESCALE((INT32) wsptr[0], PASS1_BITS+3)
				  & RANGE_MASK];
      
      outptr[0] = dcval;
      outptr[1] = dcval;
      outptr[2] = dcval;
      outptr[3] = dcval;
      
      wsptr += DCTSIZE;		/* advance pointer to next row */
      continue;
    }
#endif
    
    /* Even part */
    
    tmp0 = ((INT32) wsptr[0]) << (CONST_BITS+1);
    
    tmp2 = MULTIPLY((INT32) wsptr[2], FIX_1_847759065)
	 + MULTIPLY((INT32) wsptr[6], - FIX_0_765366865);
    
    tmp10 = tmp0 + tmp2;
    tmp12 = tmp0 - tmp2;
    
    /* Odd part */
    
    z1 = (INT32) wsptr[7];
    z2 = (INT32) wsptr[5];
    z3 = (INT32) wsptr[3];
    z4 = (INT32) wsptr[1];
    
    tmp0 = MULTIPLY(z1, - FIX_0_211164243) /* sqrt(2) * (c3-c1) */
	 + MULTIPLY(z2, FIX_1_451774981) /* sqrt(2) * (c3+c7) */
	 + MULTIPLY(z3, - FIX_2_172734803) /* sqrt(2) * (-c1-c5) */
	 + MULTIPLY(z4, FIX_1_061594337); /* sqrt(2) * (c5+c7) */
    
    tmp2 = MULTIPLY(z1, - FIX_0_509795579) /* sqrt(2) * (c7-c5) */
	 + MULTIPLY(z2, - FIX_0_601344887) /* sqrt(2) * (c5-c1) */
	 + MULTIPLY(z3, FIX_0_899976223) /* sqrt(2) * (c3-c7) */
	 + MULTIPLY(z4, FIX_2_562915447); /* sqrt(2) * (c1+c3) */

    /* Final output stage */
    
    outptr[0] = range_limit[(int) DESCALE(tmp10 + tmp2,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
    outptr[3] = range_limit[(int) DESCALE(tmp10 - tmp2,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
    outptr[1] = range_limit[(int) DESCALE(tmp12 + tmp0,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
    outptr[2] = range_limit[(int) DESCALE(tmp12 - tmp0,
					  CONST_BITS+PASS1_BITS+3+1)
			    & RANGE_MASK];
    
    wsptr += DCTSIZE;		/* advance pointer to next row */
  }
}

#endif

/*
 * Perform dequantization and inverse DCT on one block of coefficients,
 * producing a reduced-size 2x2 output block.
 */

GLOBAL(void)
jpeg_idct_2x2 (j_decompress_ptr cinfo, jpeg_component_info * compptr,
	       JCOEFPTR coef_block,
	       JSAMPARRAY output_buf, JDIMENSION output_col)
{
  INT32 tmp0, tmp10, z1;
  JCOEFPTR inptr;
  ISLOW_MULT_TYPE * quantptr;
  int * wsptr;
  JSAMPROW outptr;
  JSAMPLE *range_limit = IDCT_range_limit(cinfo);
  int ctr;
  int workspace[DCTSIZE*2];	/* buffers data between passes */
  SHIFT_TEMPS

  /* Pass 1: process columns from input, store into work array. */

  inptr = coef_block;
  quantptr = (ISLOW_MULT_TYPE *) compptr->dct_table;
  wsptr = workspace;
  for (ctr = DCTSIZE; ctr > 0; inptr++, quantptr++, wsptr++, ctr--) {
    /* Don't bother to process columns 2,4,6 */
    if (ctr == DCTSIZE-2 || ctr == DCTSIZE-4 || ctr == DCTSIZE-6)
      continue;
    if (inptr[DCTSIZE*1] == 0 && inptr[DCTSIZE*3] == 0 &&
	inptr[DCTSIZE*5] == 0 && inptr[DCTSIZE*7] == 0) {
      /* AC terms all zero; we need not examine terms 2,4,6 for 2x2 output */
      int dcval = DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]) << PASS1_BITS;
      
      wsptr[DCTSIZE*0] = dcval;
      wsptr[DCTSIZE*1] = dcval;
      
      continue;
    }
    
    /* Even part */
    
    z1 = DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]);
    tmp10 = z1 << (CONST_BITS+2);
    
    /* Odd part */

    z1 = DEQUANTIZE(inptr[DCTSIZE*7], quantptr[DCTSIZE*7]);
    tmp0 = MULTIPLY(z1, - FIX_0_720959822); /* sqrt(2) * (c7-c5+c3-c1) */
    z1 = DEQUANTIZE(inptr[DCTSIZE*5], quantptr[DCTSIZE*5]);
    tmp0 += MULTIPLY(z1, FIX_0_850430095); /* sqrt(2) * (-c1+c3+c5+c7) */
    z1 = DEQUANTIZE(inptr[DCTSIZE*3], quantptr[DCTSIZE*3]);
    tmp0 += MULTIPLY(z1, - FIX_1_272758580); /* sqrt(2) * (-c1+c3-c5-c7) */
    z1 = DEQUANTIZE(inptr[DCTSIZE*1], quantptr[DCTSIZE*1]);
    tmp0 += MULTIPLY(z1, FIX_3_624509785); /* sqrt(2) * (c1+c3+c5+c7) */

    /* Final output stage */
    
    wsptr[DCTSIZE*0] = (int) DESCALE(tmp10 + tmp0, CONST_BITS-PASS1_BITS+2);
    wsptr[DCTSIZE*1] = (int) DESCALE(tmp10 - tmp0, CONST_BITS-PASS1_BITS+2);
  }
  
  /* Pass 2: process 2 rows from work array, store into output array. */

  wsptr = workspace;
  for (ctr = 0; ctr < 2; ctr++) {
    outptr = output_buf[ctr] + output_col;
    /* It's not clear whether a zero row test is worthwhile here ... */

#ifndef NO_ZERO_ROW_TEST
    if (wsptr[1] == 0 && wsptr[3] == 0 && wsptr[5] == 0 && wsptr[7] == 0) {
      /* AC terms all zero */
      JSAMPLE dcval = range_limit[(int) DESCALE((INT32) wsptr[0], PASS1_BITS+3)
				  & RANGE_MASK];
      
      outptr[0] = dcval;
      outptr[1] = dcval;
      
      wsptr += DCTSIZE;		/* advance pointer to next row */
      continue;
    }
#endif
    
    /* Even part */
    
    tmp10 = ((INT32) wsptr[0]) << (CONST_BITS+2);
    
    /* Odd part */

    tmp0 = MULTIPLY((INT32) wsptr[7], - FIX_0_720959822) /* sqrt(2) * (c7-c5+c3-c1) */
	 + MULTIPLY((INT32) wsptr[5], FIX_0_850430095) /* sqrt(2) * (-c1+c3+c5+c7) */
	 + MULTIPLY((INT32) wsptr[3], - FIX_1_272758580) /* sqrt(2) * (-c1+c3-c5-c7) */
	 + MULTIPLY((INT32) wsptr[1], FIX_3_624509785); /* sqrt(2) * (c1+c3+c5+c7) */

    /* Final output stage */
    
    outptr[0] = range_limit[(int) DESCALE(tmp10 + tmp0,
					  CONST_BITS+PASS1_BITS+3+2)
			    & RANGE_MASK];
    outptr[1] = range_limit[(int) DESCALE(tmp10 - tmp0,
					  CONST_BITS+PASS1_BITS+3+2)
			    & RANGE_MASK];
    
    wsptr += DCTSIZE;		/* advance pointer to next row */
  }
}


/*
 * Perform dequantization and inverse DCT on one block of coefficients,
 * producing a reduced-size 1x1 output block.
 */

GLOBAL(void)
jpeg_idct_1x1 (j_decompress_ptr cinfo, jpeg_component_info * compptr,
	       JCOEFPTR coef_block,
	       JSAMPARRAY output_buf, JDIMENSION output_col)
{
  int dcval;
  ISLOW_MULT_TYPE * quantptr;
  JSAMPLE *range_limit = IDCT_range_limit(cinfo);
  SHIFT_TEMPS

  /* We hardly need an inverse DCT routine for this: just take the
   * average pixel value, which is one-eighth of the DC coefficient.
   */
  quantptr = (ISLOW_MULT_TYPE *) compptr->dct_table;
  dcval = DEQUANTIZE(coef_block[0], quantptr[0]);
  dcval = (int) DESCALE((INT32) dcval, 3);

  output_buf[0][output_col] = range_limit[dcval & RANGE_MASK];
}

#endif /* IDCT_SCALING_SUPPORTED */
