/*
 * jdcolor.c
 *
 * Copyright (C) 1991-1997, Thomas G. Lane.
 * Copyright 2009 Pierre Ossman <ossman@cendio.se> for Cendio AB
 * Copyright (C) 2009, D. R. Commander.
 *
 * ARM NEON optimizations
 * Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.
 * Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
 *
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file contains output colorspace conversion routines.
 */

#define JPEG_INTERNALS
#include "jinclude.h"
#include "jpeglib.h"
#include "jsimd.h"


/* Private subobject */

typedef struct {
  struct jpeg_color_deconverter pub; /* public fields */

  /* Private state for YCC->RGB conversion */
  int * Cr_r_tab;		/* => table for Cr to R conversion */
  int * Cb_b_tab;		/* => table for Cb to B conversion */
  INT32 * Cr_g_tab;		/* => table for Cr to G conversion */
  INT32 * Cb_g_tab;		/* => table for Cb to G conversion */
} my_color_deconverter;

typedef my_color_deconverter * my_cconvert_ptr;


/**************** YCbCr -> RGB conversion: most common case **************/

/*
 * YCbCr is defined per CCIR 601-1, except that Cb and Cr are
 * normalized to the range 0..MAXJSAMPLE rather than -0.5 .. 0.5.
 * The conversion equations to be implemented are therefore
 *	R = Y                + 1.40200 * Cr
 *	G = Y - 0.34414 * Cb - 0.71414 * Cr
 *	B = Y + 1.77200 * Cb
 * where Cb and Cr represent the incoming values less CENTERJSAMPLE.
 * (These numbers are derived from TIFF 6.0 section 21, dated 3-June-92.)
 *
 * To avoid floating-point arithmetic, we represent the fractional constants
 * as integers scaled up by 2^16 (about 4 digits precision); we have to divide
 * the products by 2^16, with appropriate rounding, to get the correct answer.
 * Notice that Y, being an integral input, does not contribute any fraction
 * so it need not participate in the rounding.
 *
 * For even more speed, we avoid doing any multiplications in the inner loop
 * by precalculating the constants times Cb and Cr for all possible values.
 * For 8-bit JSAMPLEs this is very reasonable (only 256 entries per table);
 * for 12-bit samples it is still acceptable.  It's not very reasonable for
 * 16-bit samples, but if you want lossless storage you shouldn't be changing
 * colorspace anyway.
 * The Cr=>R and Cb=>B values can be rounded to integers in advance; the
 * values for the G calculation are left scaled up, since we must add them
 * together before rounding.
 */

#define SCALEBITS	16	/* speediest right-shift on some machines */
#define ONE_HALF	((INT32) 1 << (SCALEBITS-1))
#define FIX(x)		((INT32) ((x) * (1L<<SCALEBITS) + 0.5))


/*
 * Initialize tables for YCC->RGB colorspace conversion.
 */

LOCAL(void)
build_ycc_rgb_table (j_decompress_ptr cinfo)
{
  my_cconvert_ptr cconvert = (my_cconvert_ptr) cinfo->cconvert;
  int i;
  INT32 x;
  SHIFT_TEMPS

  cconvert->Cr_r_tab = (int *)
    (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_IMAGE,
				(MAXJSAMPLE+1) * SIZEOF(int));
  cconvert->Cb_b_tab = (int *)
    (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_IMAGE,
				(MAXJSAMPLE+1) * SIZEOF(int));
  cconvert->Cr_g_tab = (INT32 *)
    (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_IMAGE,
				(MAXJSAMPLE+1) * SIZEOF(INT32));
  cconvert->Cb_g_tab = (INT32 *)
    (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_IMAGE,
				(MAXJSAMPLE+1) * SIZEOF(INT32));

  for (i = 0, x = -CENTERJSAMPLE; i <= MAXJSAMPLE; i++, x++) {
    /* i is the actual input pixel value, in the range 0..MAXJSAMPLE */
    /* The Cb or Cr value we are thinking of is x = i - CENTERJSAMPLE */
    /* Cr=>R value is nearest int to 1.40200 * x */
    cconvert->Cr_r_tab[i] = (int)
		    RIGHT_SHIFT(FIX(1.40200) * x + ONE_HALF, SCALEBITS);
    /* Cb=>B value is nearest int to 1.77200 * x */
    cconvert->Cb_b_tab[i] = (int)
		    RIGHT_SHIFT(FIX(1.77200) * x + ONE_HALF, SCALEBITS);
    /* Cr=>G value is scaled-up -0.71414 * x */
    cconvert->Cr_g_tab[i] = (- FIX(0.71414)) * x;
    /* Cb=>G value is scaled-up -0.34414 * x */
    /* We also add in ONE_HALF so that need not do it in inner loop */
    cconvert->Cb_g_tab[i] = (- FIX(0.34414)) * x + ONE_HALF;
  }
}


/*
 * Convert some rows of samples to the output colorspace.
 *
 * Note that we change from noninterleaved, one-plane-per-component format
 * to interleaved-pixel format.  The output buffer is therefore three times
 * as wide as the input buffer.
 * A starting row offset is provided only for the input buffer.  The caller
 * can easily adjust the passed output_buf value to accommodate any row
 * offset required on that side.
 */

#if defined(__ARM_NEON__) && defined(WITH_SIMD) && \
    (RGB_PIXELSIZE == 3) && (BITS_IN_JSAMPLE == 8)

LOCAL(void)
yuv444_to_rgb24_neon(unsigned char *rgb,
		     unsigned char *y, unsigned char *u, unsigned char *v,
		     int n)
{
    const static signed short c[12] = {
        22971, -11277, -23401, 29033,
        -128, -128, -128, -128, -128, -128, -128, -128
    };
    asm volatile (
        ".fpu neon\n"
        ".macro do_yuv_to_rgb\n"
        "   vaddw.u8    q3, q1, d4\n"     /* q3 = u - 128 */
        "   vaddw.u8    q4, q1, d5\n"     /* q2 = v - 128 */
        "   vmull.s16   q10, d6, d1[1]\n" /* multiply by -11277 */
        "   vmull.s16   q11, d7, d1[1]\n" /* multiply by -11277 */
        "   vmlal.s16   q10, d8, d1[2]\n" /* multiply by -23401 */
        "   vmlal.s16   q11, d9, d1[2]\n" /* multiply by -23401 */
        "   vmull.s16   q12, d8, d1[0]\n" /* multiply by 22971 */
        "   vmull.s16   q13, d9, d1[0]\n" /* multiply by 22971 */
        "   vmull.s16   q14, d6, d1[3]\n" /* multiply by 29033 */
        "   vmull.s16   q15, d7, d1[3]\n" /* multiply by 29033 */
        "   vrshrn.s32  d20, q10, #15\n"
        "   vrshrn.s32  d21, q11, #15\n"
        "   vrshrn.s32  d24, q12, #14\n"
        "   vrshrn.s32  d25, q13, #14\n"
        "   vrshrn.s32  d28, q14, #14\n"
        "   vrshrn.s32  d29, q15, #14\n"
        "   vaddw.u8    q10, q10, d0\n"
        "   vaddw.u8    q12, q12, d0\n"
        "   vaddw.u8    q14, q14, d0\n"
        "   vqmovun.s16 d11, q10\n"
        "   vqmovun.s16 d10, q12\n"
        "   vqmovun.s16 d12, q14\n"
        ".endm\n"
        ".macro do_load size\n"
            ".if \\size == 8\n"
                "vld1.8  {d4}, [%[u]]!\n"
                "vld1.8  {d5}, [%[v]]!\n"
                "vld1.8  {d0}, [%[y]]!\n"
                "pld [%[y], #64]\n"
                "pld [%[u], #64]\n"
                "pld [%[v], #64]\n"
            ".elseif \\size == 4\n"
                "vld1.8  {d4[0]}, [%[u]]!\n"
                "vld1.8  {d4[1]}, [%[u]]!\n"
                "vld1.8  {d4[2]}, [%[u]]!\n"
                "vld1.8  {d4[3]}, [%[u]]!\n"
                "vld1.8  {d5[0]}, [%[v]]!\n"
                "vld1.8  {d5[1]}, [%[v]]!\n"
                "vld1.8  {d5[2]}, [%[v]]!\n"
                "vld1.8  {d5[3]}, [%[v]]!\n"
                "vld1.8  {d0[0]}, [%[y]]!\n"
                "vld1.8  {d0[1]}, [%[y]]!\n"
                "vld1.8  {d0[2]}, [%[y]]!\n"
                "vld1.8  {d0[3]}, [%[y]]!\n"
            ".elseif \\size == 2\n"
                "vld1.8  {d4[4]}, [%[u]]!\n"
                "vld1.8  {d4[5]}, [%[u]]!\n"
                "vld1.8  {d5[4]}, [%[v]]!\n"
                "vld1.8  {d5[5]}, [%[v]]!\n"
                "vld1.8  {d0[4]}, [%[y]]!\n"
                "vld1.8  {d0[5]}, [%[y]]!\n"
            ".elseif \\size == 1\n"
                "vld1.8  {d4[6]}, [%[u]]!\n"
                "vld1.8  {d5[6]}, [%[v]]!\n"
                "vld1.8  {d0[6]}, [%[y]]!\n"
            ".else\n"
                ".error \"unsupported macroblock size\"\n"
            ".endif\n"
        ".endm\n"
        ".macro do_store size\n"
            ".if \\size == 8\n"
                "vst3.8 {d10, d11, d12}, [%[rgb]]!\n"
            ".elseif \\size == 4\n"
                "vst3.8 {d10[0], d11[0], d12[0]}, [%[rgb]]!\n"
                "vst3.8 {d10[1], d11[1], d12[1]}, [%[rgb]]!\n"
                "vst3.8 {d10[2], d11[2], d12[2]}, [%[rgb]]!\n"
                "vst3.8 {d10[3], d11[3], d12[3]}, [%[rgb]]!\n"
            ".elseif \\size == 2\n"
                "vst3.8 {d10[4], d11[4], d12[4]}, [%[rgb]]!\n"
                "vst3.8 {d10[5], d11[5], d12[5]}, [%[rgb]]!\n"
            ".elseif \\size == 1\n"
                "vst3.8 {d10[6], d11[6], d12[6]}, [%[rgb]]!\n"
            ".else\n"
                ".error \"unsupported macroblock size\"\n"
            ".endif\n"
        ".endm\n"
        "vld1.16 {d1, d2, d3}, [%[c]]\n" /* load constants */
        "subs        %[n], %[n], #8\n"
        "blt         2f\n"
    "1:\n"
        "do_load 8\n"
        "do_yuv_to_rgb\n"
        "do_store 8\n"
        "subs        %[n], %[n], #8\n"
        "bge         1b\n"
        "tst         %[n], #7\n"
        "beq         8f\n"
    "2:\n"
        "tst         %[n], #4\n"
        "beq         3f\n"
        "do_load 4\n"
    "3:\n"
        "tst         %[n], #2\n"
        "beq         4f\n"
        "do_load 2\n"
    "4:\n"
        "tst         %[n], #1\n"
        "beq         5f\n"
        "do_load 1\n"
    "5:\n"
        "do_yuv_to_rgb\n"
        "tst         %[n], #4\n"
        "beq         6f\n"
        "do_store 4\n"
    "6:\n"
        "tst         %[n], #2\n"
        "beq         7f\n"
        "do_store 2\n"
    "7:\n"
        "tst         %[n], #1\n"
        "beq         8f\n"
        "do_store 1\n"
    "8:\n"
    ".purgem do_load\n"
    ".purgem do_yuv_to_rgb\n"
    ".purgem do_store\n"
    : [rgb] "+&r" (rgb), [y] "+&r" (y), [u] "+&r" (u), [v] "+&r" (v),
      [n] "+&r" (n)
    : [c] "r" (&c[0])
    : "cc", "memory",
      "d0",  "d1",  "d2",  "d3",  "d4",  "d5",  "d6",  "d7",
      "d8",  "d9",  "d10", "d11", "d12", "d13", "d14", "d15",
      "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23",
      "d24", "d25", "d26", "d27", "d28", "d29", "d30", "d31");
}

METHODDEF(void)
ycc_rgb_convert_neon (j_decompress_ptr cinfo,
		      JSAMPIMAGE input_buf, JDIMENSION input_row,
		      JSAMPARRAY output_buf, int num_rows)
{
  register JSAMPROW outptr;
  register JSAMPROW inptr0, inptr1, inptr2;
  JDIMENSION num_cols = cinfo->output_width;

  while (--num_rows >= 0) {
    inptr0 = input_buf[0][input_row];
    inptr1 = input_buf[1][input_row];
    inptr2 = input_buf[2][input_row];
    input_row++;
    outptr = *output_buf++;
    yuv444_to_rgb24_neon(outptr, inptr0, inptr1, inptr2, num_cols);
    outptr += 3 * num_cols;
  }
}

#endif

METHODDEF(void)
ycc_rgb_convert (j_decompress_ptr cinfo,
		 JSAMPIMAGE input_buf, JDIMENSION input_row,
		 JSAMPARRAY output_buf, int num_rows)
{
  my_cconvert_ptr cconvert = (my_cconvert_ptr) cinfo->cconvert;
  register int y, cb, cr;
  register JSAMPROW outptr;
  register JSAMPROW inptr0, inptr1, inptr2;
  register JDIMENSION col;
  JDIMENSION num_cols = cinfo->output_width;
  /* copy these pointers into registers if possible */
  register JSAMPLE * range_limit = cinfo->sample_range_limit;
  register int * Crrtab = cconvert->Cr_r_tab;
  register int * Cbbtab = cconvert->Cb_b_tab;
  register INT32 * Crgtab = cconvert->Cr_g_tab;
  register INT32 * Cbgtab = cconvert->Cb_g_tab;
  SHIFT_TEMPS

  while (--num_rows >= 0) {
    inptr0 = input_buf[0][input_row];
    inptr1 = input_buf[1][input_row];
    inptr2 = input_buf[2][input_row];
    input_row++;
    outptr = *output_buf++;
    for (col = 0; col < num_cols; col++) {
      y  = GETJSAMPLE(inptr0[col]);
      cb = GETJSAMPLE(inptr1[col]);
      cr = GETJSAMPLE(inptr2[col]);
      /* Range-limiting is essential due to noise introduced by DCT losses. */
      outptr[rgb_red[cinfo->out_color_space]] =   range_limit[y + Crrtab[cr]];
      outptr[rgb_green[cinfo->out_color_space]] = range_limit[y +
			      ((int) RIGHT_SHIFT(Cbgtab[cb] + Crgtab[cr],
						 SCALEBITS))];
      outptr[rgb_blue[cinfo->out_color_space]] =  range_limit[y + Cbbtab[cb]];
      outptr += rgb_pixelsize[cinfo->out_color_space];
    }
  }
}


/**************** Cases other than YCbCr -> RGB **************/


/*
 * Color conversion for no colorspace change: just copy the data,
 * converting from separate-planes to interleaved representation.
 */

METHODDEF(void)
null_convert (j_decompress_ptr cinfo,
	      JSAMPIMAGE input_buf, JDIMENSION input_row,
	      JSAMPARRAY output_buf, int num_rows)
{
  register JSAMPROW inptr, outptr;
  register JDIMENSION count;
  register int num_components = cinfo->num_components;
  JDIMENSION num_cols = cinfo->output_width;
  int ci;

  while (--num_rows >= 0) {
    for (ci = 0; ci < num_components; ci++) {
      inptr = input_buf[ci][input_row];
      outptr = output_buf[0] + ci;
      for (count = num_cols; count > 0; count--) {
	*outptr = *inptr++;	/* needn't bother with GETJSAMPLE() here */
	outptr += num_components;
      }
    }
    input_row++;
    output_buf++;
  }
}


/*
 * Color conversion for grayscale: just copy the data.
 * This also works for YCbCr -> grayscale conversion, in which
 * we just copy the Y (luminance) component and ignore chrominance.
 */

METHODDEF(void)
grayscale_convert (j_decompress_ptr cinfo,
		   JSAMPIMAGE input_buf, JDIMENSION input_row,
		   JSAMPARRAY output_buf, int num_rows)
{
  jcopy_sample_rows(input_buf[0], (int) input_row, output_buf, 0,
		    num_rows, cinfo->output_width);
}


/*
 * Convert grayscale to RGB: just duplicate the graylevel three times.
 * This is provided to support applications that don't want to cope
 * with grayscale as a separate case.
 */

METHODDEF(void)
gray_rgb_convert (j_decompress_ptr cinfo,
		  JSAMPIMAGE input_buf, JDIMENSION input_row,
		  JSAMPARRAY output_buf, int num_rows)
{
  register JSAMPROW inptr, outptr;
  JSAMPLE *maxinptr;
  register JDIMENSION col;
  JDIMENSION num_cols = cinfo->output_width;
  int rindex = rgb_red[cinfo->out_color_space];
  int gindex = rgb_green[cinfo->out_color_space];
  int bindex = rgb_blue[cinfo->out_color_space];
  int rgbstride = rgb_pixelsize[cinfo->out_color_space];

  while (--num_rows >= 0) {
    inptr = input_buf[0][input_row++];
    maxinptr = &inptr[num_cols];
    outptr = *output_buf++;
    for (; inptr < maxinptr; inptr++, outptr += rgbstride) {
      /* We can dispense with GETJSAMPLE() here */
      outptr[rindex] = outptr[gindex] = outptr[bindex] = *inptr;
    }
  }
}


/*
 * Adobe-style YCCK->CMYK conversion.
 * We convert YCbCr to R=1-C, G=1-M, and B=1-Y using the same
 * conversion as above, while passing K (black) unchanged.
 * We assume build_ycc_rgb_table has been called.
 */

METHODDEF(void)
ycck_cmyk_convert (j_decompress_ptr cinfo,
		   JSAMPIMAGE input_buf, JDIMENSION input_row,
		   JSAMPARRAY output_buf, int num_rows)
{
  my_cconvert_ptr cconvert = (my_cconvert_ptr) cinfo->cconvert;
  register int y, cb, cr;
  register JSAMPROW outptr;
  register JSAMPROW inptr0, inptr1, inptr2, inptr3;
  register JDIMENSION col;
  JDIMENSION num_cols = cinfo->output_width;
  /* copy these pointers into registers if possible */
  register JSAMPLE * range_limit = cinfo->sample_range_limit;
  register int * Crrtab = cconvert->Cr_r_tab;
  register int * Cbbtab = cconvert->Cb_b_tab;
  register INT32 * Crgtab = cconvert->Cr_g_tab;
  register INT32 * Cbgtab = cconvert->Cb_g_tab;
  SHIFT_TEMPS

  while (--num_rows >= 0) {
    inptr0 = input_buf[0][input_row];
    inptr1 = input_buf[1][input_row];
    inptr2 = input_buf[2][input_row];
    inptr3 = input_buf[3][input_row];
    input_row++;
    outptr = *output_buf++;
    for (col = 0; col < num_cols; col++) {
      y  = GETJSAMPLE(inptr0[col]);
      cb = GETJSAMPLE(inptr1[col]);
      cr = GETJSAMPLE(inptr2[col]);
      /* Range-limiting is essential due to noise introduced by DCT losses. */
      outptr[0] = range_limit[MAXJSAMPLE - (y + Crrtab[cr])];	/* red */
      outptr[1] = range_limit[MAXJSAMPLE - (y +			/* green */
			      ((int) RIGHT_SHIFT(Cbgtab[cb] + Crgtab[cr],
						 SCALEBITS)))];
      outptr[2] = range_limit[MAXJSAMPLE - (y + Cbbtab[cb])];	/* blue */
      /* K passes through unchanged */
      outptr[3] = inptr3[col];	/* don't need GETJSAMPLE here */
      outptr += 4;
    }
  }
}


/*
 * Empty method for start_pass.
 */

METHODDEF(void)
start_pass_dcolor (j_decompress_ptr cinfo)
{
  /* no work needed */
}


/*
 * Module initialization routine for output colorspace conversion.
 */

GLOBAL(void)
jinit_color_deconverter (j_decompress_ptr cinfo)
{
  my_cconvert_ptr cconvert;
  int ci;

  cconvert = (my_cconvert_ptr)
    (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_IMAGE,
				SIZEOF(my_color_deconverter));
  cinfo->cconvert = (struct jpeg_color_deconverter *) cconvert;
  cconvert->pub.start_pass = start_pass_dcolor;

  /* Make sure num_components agrees with jpeg_color_space */
  switch (cinfo->jpeg_color_space) {
  case JCS_GRAYSCALE:
    if (cinfo->num_components != 1)
      ERREXIT(cinfo, JERR_BAD_J_COLORSPACE);
    break;

  case JCS_RGB:
  case JCS_YCbCr:
    if (cinfo->num_components != 3)
      ERREXIT(cinfo, JERR_BAD_J_COLORSPACE);
    break;

  case JCS_CMYK:
  case JCS_YCCK:
    if (cinfo->num_components != 4)
      ERREXIT(cinfo, JERR_BAD_J_COLORSPACE);
    break;

  default:			/* JCS_UNKNOWN can be anything */
    if (cinfo->num_components < 1)
      ERREXIT(cinfo, JERR_BAD_J_COLORSPACE);
    break;
  }

  /* Set out_color_components and conversion method based on requested space.
   * Also clear the component_needed flags for any unused components,
   * so that earlier pipeline stages can avoid useless computation.
   */

  switch (cinfo->out_color_space) {
  case JCS_GRAYSCALE:
    cinfo->out_color_components = 1;
    if (cinfo->jpeg_color_space == JCS_GRAYSCALE ||
	cinfo->jpeg_color_space == JCS_YCbCr) {
      cconvert->pub.color_convert = grayscale_convert;
      /* For color->grayscale conversion, only the Y (0) component is needed */
      for (ci = 1; ci < cinfo->num_components; ci++)
	cinfo->comp_info[ci].component_needed = FALSE;
    } else
      ERREXIT(cinfo, JERR_CONVERSION_NOTIMPL);
    break;

  case JCS_RGB:
  case JCS_EXT_RGB:
#if defined(__ARM_NEON__) && defined(WITH_SIMD) && \
    (RGB_PIXELSIZE == 3) && (BITS_IN_JSAMPLE == 8)
    if (cinfo->jpeg_color_space == JCS_YCbCr) {
        cconvert->pub.color_convert = ycc_rgb_convert_neon;
        break;
    }
#endif
  case JCS_EXT_RGBX:
  case JCS_EXT_BGR:
  case JCS_EXT_BGRX:
  case JCS_EXT_XBGR:
  case JCS_EXT_XRGB:
    cinfo->out_color_components = rgb_pixelsize[cinfo->out_color_space];
    if (cinfo->jpeg_color_space == JCS_YCbCr) {
      if (jsimd_can_ycc_rgb())
        cconvert->pub.color_convert = jsimd_ycc_rgb_convert;
      else {
        cconvert->pub.color_convert = ycc_rgb_convert;
        build_ycc_rgb_table(cinfo);
      }
    } else if (cinfo->jpeg_color_space == JCS_GRAYSCALE) {
      cconvert->pub.color_convert = gray_rgb_convert;
    } else if (cinfo->jpeg_color_space == cinfo->out_color_space &&
      rgb_pixelsize[cinfo->out_color_space] == 3) {
      cconvert->pub.color_convert = null_convert;
    } else
      ERREXIT(cinfo, JERR_CONVERSION_NOTIMPL);
    break;

  case JCS_CMYK:
    cinfo->out_color_components = 4;
    if (cinfo->jpeg_color_space == JCS_YCCK) {
      cconvert->pub.color_convert = ycck_cmyk_convert;
      build_ycc_rgb_table(cinfo);
    } else if (cinfo->jpeg_color_space == JCS_CMYK) {
      cconvert->pub.color_convert = null_convert;
    } else
      ERREXIT(cinfo, JERR_CONVERSION_NOTIMPL);
    break;

  default:
    /* Permit null conversion to same output space */
    if (cinfo->out_color_space == cinfo->jpeg_color_space) {
      cinfo->out_color_components = cinfo->num_components;
      cconvert->pub.color_convert = null_convert;
    } else			/* unsupported non-null conversion */
      ERREXIT(cinfo, JERR_CONVERSION_NOTIMPL);
    break;
  }

  if (cinfo->quantize_colors)
    cinfo->output_components = 1; /* single colormapped output component */
  else
    cinfo->output_components = cinfo->out_color_components;
}
