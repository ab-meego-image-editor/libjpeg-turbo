/*
 * jfdctfst.c
 *
 * Copyright (C) 1994-1996, Thomas G. Lane.
 *
 * ARM NEON optimizations
 * Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.
 * Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
 *
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file contains a fast, not so accurate integer implementation of the
 * forward DCT (Discrete Cosine Transform).
 *
 * A 2-D DCT can be done by 1-D DCT on each row followed by 1-D DCT
 * on each column.  Direct algorithms are also available, but they are
 * much more complex and seem not to be any faster when reduced to code.
 *
 * This implementation is based on Arai, Agui, and Nakajima's algorithm for
 * scaled DCT.  Their original paper (Trans. IEICE E-71(11):1095) is in
 * Japanese, but the algorithm is described in the Pennebaker & Mitchell
 * JPEG textbook (see REFERENCES section in file README).  The following code
 * is based directly on figure 4-8 in P&M.
 * While an 8-point DCT cannot be done in less than 11 multiplies, it is
 * possible to arrange the computation so that many of the multiplies are
 * simple scalings of the final outputs.  These multiplies can then be
 * folded into the multiplications or divisions by the JPEG quantization
 * table entries.  The AA&N method leaves only 5 multiplies and 29 adds
 * to be done in the DCT itself.
 * The primary disadvantage of this method is that with fixed-point math,
 * accuracy is lost due to imprecise representation of the scaled
 * quantization values.  The smaller the quantization table entry, the less
 * precise the scaled value, so this implementation does worse with high-
 * quality-setting files than with low-quality ones.
 */

#define JPEG_INTERNALS
#include "jinclude.h"
#include "jpeglib.h"
#include "jdct.h"		/* Private declarations for DCT subsystem */

#ifdef DCT_IFAST_SUPPORTED


/*
 * This module is specialized to the case DCTSIZE = 8.
 */

#if DCTSIZE != 8
  Sorry, this code only copes with 8x8 DCTs. /* deliberate syntax err */
#endif


/* Scaling decisions are generally the same as in the LL&M algorithm;
 * see jfdctint.c for more details.  However, we choose to descale
 * (right shift) multiplication products as soon as they are formed,
 * rather than carrying additional fractional bits into subsequent additions.
 * This compromises accuracy slightly, but it lets us save a few shifts.
 * More importantly, 16-bit arithmetic is then adequate (for 8-bit samples)
 * everywhere except in the multiplications proper; this saves a good deal
 * of work on 16-bit-int machines.
 *
 * Again to save a few shifts, the intermediate results between pass 1 and
 * pass 2 are not upscaled, but are represented only to integral precision.
 *
 * A final compromise is to represent the multiplicative constants to only
 * 8 fractional bits, rather than 13.  This saves some shifting work on some
 * machines, and may also reduce the cost of multiplication (since there
 * are fewer one-bits in the constants).
 */

#define CONST_BITS  8


/* Some C compilers fail to reduce "FIX(constant)" at compile time, thus
 * causing a lot of useless floating-point operations at run time.
 * To get around this we use the following pre-calculated constants.
 * If you change CONST_BITS you may want to add appropriate values.
 * (With a reasonable C compiler, you can just rely on the FIX() macro...)
 */

#if CONST_BITS == 8
#define FIX_0_382683433  ((INT32)   98)		/* FIX(0.382683433) */
#define FIX_0_541196100  ((INT32)  139)		/* FIX(0.541196100) */
#define FIX_0_707106781  ((INT32)  181)		/* FIX(0.707106781) */
#define FIX_1_306562965  ((INT32)  334)		/* FIX(1.306562965) */
#else
#define FIX_0_382683433  FIX(0.382683433)
#define FIX_0_541196100  FIX(0.541196100)
#define FIX_0_707106781  FIX(0.707106781)
#define FIX_1_306562965  FIX(1.306562965)
#endif


/* We can gain a little more speed, with a further compromise in accuracy,
 * by omitting the addition in a descaling shift.  This yields an incorrectly
 * rounded result half the time...
 */

#ifndef USE_ACCURATE_ROUNDING
#undef DESCALE
#define DESCALE(x,n)  RIGHT_SHIFT(x, n)
#endif


/* Multiply a DCTELEM variable by an INT32 constant, and immediately
 * descale to yield a DCTELEM result.
 */

#define MULTIPLY(var,const)  ((DCTELEM) DESCALE((var) * (const), CONST_BITS))


/*
 * Perform the forward DCT on one block of samples.
 */

#if defined(WITH_SIMD) && defined(__ARM_NEON__) && (BITS_IN_JSAMPLE == 8)

#define XFIX_0_382683433  ((short)(98 * 128))
#define XFIX_0_541196100  ((short)(139 * 128))
#define XFIX_0_707106781  ((short)(181 * 128))
#define XFIX_1_306562965  ((short)(334 * 128 - 256 * 128))

/* Macro which is similar to VQDMULH NEON instruction */
#define XMULTIPLY(var,const)  ((DCTELEM) DESCALE((var) * (const) * 2, 16))

GLOBAL(void)
jpeg_fdct_ifast_neon (DCTELEM * data)
{
#if 1
  JCOEFPTR inptr, outptr;
  const static short c[4] = {
    XFIX_0_382683433, /* d0[0] */
    XFIX_0_541196100, /* d0[1] */
    XFIX_0_707106781, /* d0[2] */
    XFIX_1_306562965  /* d0[3] */
  };

  inptr = outptr = data;
  asm volatile (
    /* load constants */
    "vld1.16 {d0}, [%[c]]\n"
    /* load all coef block:
     *   0 | d4  d5
     *   1 | d6  d7
     *   2 | d8  d9
     *   3 | d10 d11
     *   4 | d12 d13
     *   5 | d14 d15
     *   6 | d16 d17
     *   7 | d18 d19
     */
    "vld1.16 {d4, d5, d6, d7}, [%[inptr]]!\n"
    "vld1.16 {d8, d9, d10, d11}, [%[inptr]]!\n"
    "vld1.16 {d12, d13, d14, d15}, [%[inptr]]!\n"
    "vld1.16 {d16, d17, d18, d19}, [%[inptr]]!\n"

    ".macro transpose_4x4 x0, x1, x2, x3\n"
    "vtrn.16 \\x0, \\x1\n"
    "vtrn.16 \\x2, \\x3\n"
    "vtrn.32 \\x0, \\x2\n"
    "vtrn.32 \\x1, \\x3\n"
    ".endm\n"

    ".macro transpose_all\n"
    "transpose_4x4  d4,  d6,  d8,  d10\n"
    "transpose_4x4  d5,  d7,  d9,  d11\n"
    "transpose_4x4  d12, d14, d16, d18\n"
    "transpose_4x4  d13, d15, d17, d19\n"
    "vswp           d12, d5\n"
    "vswp           d14, d7\n"
    "vswp           d16, d9\n"
    "vswp           d18, d11\n"
    ".endm\n"

    ".macro idct_helper x0, x1, x2, x3, x4, x5, x6, x7,"
    "                   t10, t11, t12, t13, t14, t15\n"
    /* preprocess input data, converting them to sums and differences */
    "vsub.s16     \\t10, \\x0, \\x7\n"
    "vadd.s16     \\x0,  \\x0, \\x7\n"
    "vswp.s16     \\t10, \\x7\n"
    "vsub.s16     \\t11, \\x1, \\x6\n"
    "vadd.s16     \\x1,  \\x1, \\x6\n"
    "vswp.s16     \\t11, \\x6\n"
    "vsub.s16     \\t10, \\x2, \\x5\n"
    "vadd.s16     \\x2,  \\x2, \\x5\n"
    "vswp.s16     \\t10, \\x5\n"
    "vsub.s16     \\t11, \\x3, \\x4\n"
    "vadd.s16     \\x3,  \\x3, \\x4\n"
    "vswp.s16     \\t11, \\x4\n"

    "vsub.s16     \\t12, \\x1,  \\x2\n"  /* tmp12 = dataptr[1] - dataptr[2]; */
    "vsub.s16     \\t13, \\x0,  \\x3\n"  /* tmp13 = dataptr[0] - dataptr[3]; */
    "vadd.s16     \\t12, \\t12, \\t13\n" /* tmp12 = tmp12 + tmp13; */
    "vqdmulh.s16  \\t12, \\t12, d0[2]\n" /* tmp12 = XMULTIPLY(tmp12, XFIX_0_707106781); */
    "vadd.s16     \\t11, \\x1,  \\x2\n"  /* tmp11 = dataptr[1] + dataptr[2]; */
    "vadd.s16     \\t10, \\x0,  \\x3\n"  /* tmp10 = dataptr[0] + dataptr[3]; */

    "vadd.s16     \\x3,  \\x4,  \\x5\n"  /* wsptr[3*DCTSIZE] = dataptr[4] + dataptr[5]; */
    "vadd.s16     \\x0,  \\t10, \\t11\n" /* wsptr[0*DCTSIZE] = tmp10 + tmp11; */
    "vsub.s16     \\x4,  \\t10, \\t11\n" /* wsptr[4*DCTSIZE] = tmp10 - tmp11; */
    "vadd.s16     \\t11, \\x5,  \\x6\n"  /* tmp11 = dataptr[5] + dataptr[6]; */
    "vqdmulh.s16  \\t11, \\t11, d0[2]\n" /* tmp11 = XMULTIPLY(tmp11, XFIX_0_707106781); */
    "vadd.s16     \\t15, \\x6,  \\x7\n"  /* tmp15 = dataptr[6] + dataptr[7]; */
    "vsub.s16     \\t14, \\x3,  \\t15\n" /* tmp14 = wsptr[3*DCTSIZE] - tmp15; */
    "vqdmulh.s16  \\t14, \\t14, d0[0]\n" /* tmp14 = XMULTIPLY(tmp14, XFIX_0_382683433); */
    "vadd.s16     \\x2,  \\t13, \\t12\n" /* wsptr[2*DCTSIZE] = tmp13 + tmp12; */
    "vsub.s16     \\x6,  \\t13, \\t12\n" /* wsptr[6*DCTSIZE] = tmp13 - tmp12; */
    "vqdmulh.s16  \\t13, \\t15, d0[3]\n" /* tmp13 = XMULTIPLY(tmp15, XFIX_1_306562965); */
    "vqdmulh.s16  \\x3,  \\x3,  d0[1]\n" /* wsptr[3*DCTSIZE] = XMULTIPLY(wsptr[3*DCTSIZE], XFIX_0_541196100); */

    "vadd.s16     \\t12, \\x7,  \\t11\n" /* tmp12 = dataptr[7] + tmp11; */
    "vsub.s16     \\t11, \\x7,  \\t11\n" /* tmp11 = dataptr[7] - tmp11; */
    "vadd.s16     \\t15, \\t15, \\t14\n" /* tmp15 = tmp15 + tmp14; */
    "vadd.s16     \\t15, \\t15, \\t13\n" /* tmp15 = tmp15 + tmp13; */
    "vadd.s16     \\x3,  \\x3,  \\t14\n" /* wsptr[3*DCTSIZE] = wsptr[3*DCTSIZE] + tmp14; */

    "vadd.s16     \\x5, \\t11, \\x3\n"   /* wsptr[5*DCTSIZE] = tmp11 + wsptr[3*DCTSIZE]; */
    "vsub.s16     \\x3, \\t11, \\x3\n"   /* wsptr[3*DCTSIZE] = tmp11 - wsptr[3*DCTSIZE]; */
    "vadd.s16     \\x1, \\t12, \\t15\n"  /* wsptr[1*DCTSIZE] = tmp12 + tmp15; */
    "vsub.s16     \\x7, \\t12, \\t15\n"  /* wsptr[7*DCTSIZE] = tmp12 - tmp15; */
    ".endm\n"

    "transpose_all\n"
    "idct_helper q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15\n"
    "transpose_all\n"
    "idct_helper q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15\n"

    /* store results to the output buffer */
    "vst1.16 {d4, d5, d6, d7}, [%[outptr]]!\n"
    "vst1.16 {d8, d9, d10, d11}, [%[outptr]]!\n"
    "vst1.16 {d12, d13, d14, d15}, [%[outptr]]!\n"
    "vst1.16 {d16, d17, d18, d19}, [%[outptr]]!\n"

    : [inptr] "+&r" (inptr),
      [outptr] "+&r" (outptr)
    : [c] "r" (c)
    : "cc", "memory",
      "d0",  "d1",  "d2",  "d3",  "d4",  "d5",  "d6",  "d7",
      "d8",  "d9",  "d10", "d11", "d12", "d13", "d14", "d15",
      "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23",
      "d24", "d25", "d26", "d27", "d28", "d29", "d30", "d31");
#else
  DCTELEM tmp10, tmp11, tmp12, tmp13, tmp14, tmp15;
  DCTELEM *dataptr, *wsptr;
  DCTELEM workspace[64];
  int ctr;
  /* Pass 1: process rows. */

  /* preprocess input data, converting them to sums and differences */
  dataptr = data;
  for (ctr = 0; ctr < 8; ctr++, dataptr += DCTSIZE) {
    int sum  = dataptr[0] + dataptr[7];
    int diff = dataptr[0] - dataptr[7];
    dataptr[7] = diff;
    dataptr[0] = sum;
    sum  = dataptr[1] + dataptr[6];
    diff = dataptr[1] - dataptr[6];
    dataptr[6] = diff;
    dataptr[1] = sum;
    sum  = dataptr[2] + dataptr[5];
    diff = dataptr[2] - dataptr[5];
    dataptr[5] = diff;
    dataptr[2] = sum;
    sum  = dataptr[3] + dataptr[4];
    diff = dataptr[3] - dataptr[4];
    dataptr[4] = diff;
    dataptr[3] = sum;
  }

  dataptr = data;
  wsptr = workspace;
  for (ctr = DCTSIZE-1; ctr >= 0; ctr--) {
    tmp12 = dataptr[1] - dataptr[2];
    tmp13 = dataptr[0] - dataptr[3];
    tmp12 = tmp12 + tmp13;
    tmp12 = XMULTIPLY(tmp12, XFIX_0_707106781);
    tmp11 = dataptr[1] + dataptr[2];
    tmp10 = dataptr[0] + dataptr[3];

    wsptr[3*DCTSIZE] = dataptr[4] + dataptr[5];
    wsptr[0*DCTSIZE] = tmp10 + tmp11;
    wsptr[4*DCTSIZE] = tmp10 - tmp11;
    tmp11 = dataptr[5] + dataptr[6];
    tmp11 = XMULTIPLY(tmp11, XFIX_0_707106781);
    tmp15 = dataptr[6] + dataptr[7];
    tmp14 = wsptr[3*DCTSIZE] - tmp15;
    tmp14 = XMULTIPLY(tmp14, XFIX_0_382683433);
    wsptr[2*DCTSIZE] = tmp13 + tmp12;
    wsptr[6*DCTSIZE] = tmp13 - tmp12;
    tmp13 = XMULTIPLY(tmp15, XFIX_1_306562965);
    wsptr[3*DCTSIZE] = XMULTIPLY(wsptr[3*DCTSIZE], XFIX_0_541196100);

    tmp12 = dataptr[7] + tmp11;
    tmp11 = dataptr[7] - tmp11;
    tmp15 = tmp15 + tmp14;
    tmp15 = tmp15 + tmp13;
    wsptr[3*DCTSIZE] = wsptr[3*DCTSIZE] + tmp14;

    wsptr[5*DCTSIZE] = tmp11 + wsptr[3*DCTSIZE];
    wsptr[3*DCTSIZE] = tmp11 - wsptr[3*DCTSIZE];
    wsptr[1*DCTSIZE] = tmp12 + tmp15;
    wsptr[7*DCTSIZE] = tmp12 - tmp15;

    dataptr += DCTSIZE;			/* advance pointer to next column */
    wsptr++;
  }

  /* Pass 2: process columns. */

  /* preprocess input data, converting them to sums and differences */
  dataptr = workspace;
  for (ctr = 0; ctr < 8; ctr++, dataptr += DCTSIZE) {
    int sum  = dataptr[0] + dataptr[7];
    int diff = dataptr[0] - dataptr[7];
    dataptr[7] = diff;
    dataptr[0] = sum;
    sum  = dataptr[1] + dataptr[6];
    diff = dataptr[1] - dataptr[6];
    dataptr[6] = diff;
    dataptr[1] = sum;
    sum  = dataptr[2] + dataptr[5];
    diff = dataptr[2] - dataptr[5];
    dataptr[5] = diff;
    dataptr[2] = sum;
    sum  = dataptr[3] + dataptr[4];
    diff = dataptr[3] - dataptr[4];
    dataptr[4] = diff;
    dataptr[3] = sum;
  }

  dataptr = workspace;
  wsptr = data;
  for (ctr = DCTSIZE-1; ctr >= 0; ctr--) {
    tmp12 = dataptr[1] - dataptr[2];
    tmp13 = dataptr[0] - dataptr[3];
    tmp12 = tmp12 + tmp13;
    tmp12 = XMULTIPLY(tmp12, XFIX_0_707106781);
    tmp11 = dataptr[1] + dataptr[2];
    tmp10 = dataptr[0] + dataptr[3];

    wsptr[3*DCTSIZE] = dataptr[4] + dataptr[5];
    wsptr[0*DCTSIZE] = tmp10 + tmp11;
    wsptr[4*DCTSIZE] = tmp10 - tmp11;
    tmp11 = dataptr[5] + dataptr[6];
    tmp11 = XMULTIPLY(tmp11, XFIX_0_707106781);
    tmp15 = dataptr[6] + dataptr[7];
    tmp14 = wsptr[3*DCTSIZE] - tmp15;
    tmp14 = XMULTIPLY(tmp14, XFIX_0_382683433);
    wsptr[2*DCTSIZE] = tmp13 + tmp12;
    wsptr[6*DCTSIZE] = tmp13 - tmp12;
    tmp13 = XMULTIPLY(tmp15, XFIX_1_306562965);
    wsptr[3*DCTSIZE] = XMULTIPLY(wsptr[3*DCTSIZE], XFIX_0_541196100);

    tmp12 = dataptr[7] + tmp11;
    tmp11 = dataptr[7] - tmp11;
    tmp15 = tmp15 + tmp14;
    tmp15 = tmp15 + tmp13;
    wsptr[3*DCTSIZE] = wsptr[3*DCTSIZE] + tmp14;

    wsptr[5*DCTSIZE] = tmp11 + wsptr[3*DCTSIZE];
    wsptr[3*DCTSIZE] = tmp11 - wsptr[3*DCTSIZE];
    wsptr[1*DCTSIZE] = tmp12 + tmp15;
    wsptr[7*DCTSIZE] = tmp12 - tmp15;

    dataptr += DCTSIZE;			/* advance pointer to next column */
    wsptr++;
  }
#endif
}

#define jpeg_fdct_ifast jpeg_fdct_ifast_neon

#else

GLOBAL(void)
jpeg_fdct_ifast (DCTELEM * data)
{
  DCTELEM tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7;
  DCTELEM tmp10, tmp11, tmp12, tmp13;
  DCTELEM z1, z2, z3, z4, z5, z11, z13;
  DCTELEM *dataptr;
  int ctr;
  SHIFT_TEMPS

  /* Pass 1: process rows. */

  dataptr = data;
  for (ctr = DCTSIZE-1; ctr >= 0; ctr--) {
    tmp0 = dataptr[0] + dataptr[7];
    tmp7 = dataptr[0] - dataptr[7];
    tmp1 = dataptr[1] + dataptr[6];
    tmp6 = dataptr[1] - dataptr[6];
    tmp2 = dataptr[2] + dataptr[5];
    tmp5 = dataptr[2] - dataptr[5];
    tmp3 = dataptr[3] + dataptr[4];
    tmp4 = dataptr[3] - dataptr[4];
    
    /* Even part */
    
    tmp10 = tmp0 + tmp3;	/* phase 2 */
    tmp13 = tmp0 - tmp3;
    tmp11 = tmp1 + tmp2;
    tmp12 = tmp1 - tmp2;
    
    dataptr[0] = tmp10 + tmp11; /* phase 3 */
    dataptr[4] = tmp10 - tmp11;
    
    z1 = MULTIPLY(tmp12 + tmp13, FIX_0_707106781); /* c4 */
    dataptr[2] = tmp13 + z1;	/* phase 5 */
    dataptr[6] = tmp13 - z1;
    
    /* Odd part */

    tmp10 = tmp4 + tmp5;	/* phase 2 */
    tmp11 = tmp5 + tmp6;
    tmp12 = tmp6 + tmp7;

    /* The rotator is modified from fig 4-8 to avoid extra negations. */
    z5 = MULTIPLY(tmp10 - tmp12, FIX_0_382683433); /* c6 */
    z2 = MULTIPLY(tmp10, FIX_0_541196100) + z5; /* c2-c6 */
    z4 = MULTIPLY(tmp12, FIX_1_306562965) + z5; /* c2+c6 */
    z3 = MULTIPLY(tmp11, FIX_0_707106781); /* c4 */

    z11 = tmp7 + z3;		/* phase 5 */
    z13 = tmp7 - z3;

    dataptr[5] = z13 + z2;	/* phase 6 */
    dataptr[3] = z13 - z2;
    dataptr[1] = z11 + z4;
    dataptr[7] = z11 - z4;

    dataptr += DCTSIZE;		/* advance pointer to next row */
  }

  /* Pass 2: process columns. */

  dataptr = data;
  for (ctr = DCTSIZE-1; ctr >= 0; ctr--) {
    tmp0 = dataptr[DCTSIZE*0] + dataptr[DCTSIZE*7];
    tmp7 = dataptr[DCTSIZE*0] - dataptr[DCTSIZE*7];
    tmp1 = dataptr[DCTSIZE*1] + dataptr[DCTSIZE*6];
    tmp6 = dataptr[DCTSIZE*1] - dataptr[DCTSIZE*6];
    tmp2 = dataptr[DCTSIZE*2] + dataptr[DCTSIZE*5];
    tmp5 = dataptr[DCTSIZE*2] - dataptr[DCTSIZE*5];
    tmp3 = dataptr[DCTSIZE*3] + dataptr[DCTSIZE*4];
    tmp4 = dataptr[DCTSIZE*3] - dataptr[DCTSIZE*4];
    
    /* Even part */
    
    tmp10 = tmp0 + tmp3;	/* phase 2 */
    tmp13 = tmp0 - tmp3;
    tmp11 = tmp1 + tmp2;
    tmp12 = tmp1 - tmp2;
    
    dataptr[DCTSIZE*0] = tmp10 + tmp11; /* phase 3 */
    dataptr[DCTSIZE*4] = tmp10 - tmp11;
    
    z1 = MULTIPLY(tmp12 + tmp13, FIX_0_707106781); /* c4 */
    dataptr[DCTSIZE*2] = tmp13 + z1; /* phase 5 */
    dataptr[DCTSIZE*6] = tmp13 - z1;
    
    /* Odd part */

    tmp10 = tmp4 + tmp5;	/* phase 2 */
    tmp11 = tmp5 + tmp6;
    tmp12 = tmp6 + tmp7;

    /* The rotator is modified from fig 4-8 to avoid extra negations. */
    z5 = MULTIPLY(tmp10 - tmp12, FIX_0_382683433); /* c6 */
    z2 = MULTIPLY(tmp10, FIX_0_541196100) + z5; /* c2-c6 */
    z4 = MULTIPLY(tmp12, FIX_1_306562965) + z5; /* c2+c6 */
    z3 = MULTIPLY(tmp11, FIX_0_707106781); /* c4 */

    z11 = tmp7 + z3;		/* phase 5 */
    z13 = tmp7 - z3;

    dataptr[DCTSIZE*5] = z13 + z2; /* phase 6 */
    dataptr[DCTSIZE*3] = z13 - z2;
    dataptr[DCTSIZE*1] = z11 + z4;
    dataptr[DCTSIZE*7] = z11 - z4;

    dataptr++;			/* advance pointer to next column */
  }
}

#endif

#endif /* DCT_IFAST_SUPPORTED */
